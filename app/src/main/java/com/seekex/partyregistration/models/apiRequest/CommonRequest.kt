package com.seekx.webService.models.apiRequest


data class CommonRequest(var keyValue: String,
                         var id: Long,
                         var expertId: Long?,
                         var type: Long,
                         var page: Int?=null){

}
