
package com.seekex.partyregistration.models.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

public class LoginResponse2 extends ResponseBase {
    @SerializedName("id")
    private String id;
    @SerializedName("dept_id")
    private String dept_id;
    @SerializedName("desg_id")
    private String desg_id;
    @SerializedName("email")
    private String email;
    @SerializedName("profile_image_thumbnail")
    private String profile_image_thumbnail;
    @SerializedName("name")
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDept_id() {
        return dept_id;
    }

    public void setDept_id(String dept_id) {
        this.dept_id = dept_id;
    }

    public String getDesg_id() {
        return desg_id;
    }

    public void setDesg_id(String desg_id) {
        this.desg_id = desg_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile_image_thumbnail() {
        return profile_image_thumbnail;
    }

    public void setProfile_image_thumbnail(String profile_image_thumbnail) {
        this.profile_image_thumbnail = profile_image_thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

