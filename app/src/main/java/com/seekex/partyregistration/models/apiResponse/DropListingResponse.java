
package com.seekex.partyregistration.models.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

import java.util.ArrayList;

public class DropListingResponse extends ResponseBase {
    @SerializedName("data")
    private ArrayList<DropListingDTO> data;

    public ArrayList<DropListingDTO> getData() {
        return data;
    }

    public void setData(ArrayList<DropListingDTO> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

