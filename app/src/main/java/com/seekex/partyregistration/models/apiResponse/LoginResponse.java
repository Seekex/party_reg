
package com.seekex.partyregistration.models.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

import okhttp3.Response;

public class LoginResponse extends ResponseBase {
    @SerializedName("User")
    private LoginResponse2 User;

    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginResponse2 getUser() {
        return User;
    }

    public void setUser(LoginResponse2 user) {
        User = user;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

