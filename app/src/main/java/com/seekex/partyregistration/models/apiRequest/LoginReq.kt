package com.seekx.webService.models.apiRequest

import com.seekex.partyregistration.retrofitclasses.RequestBase

data class LoginReq(
    var gcm_reg_no: String,
    var otp: String): RequestBase(){
    constructor():this("","")
}
