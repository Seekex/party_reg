package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class SaveJobWorkdataReq extends RequestBase {

    private SaveJobWorkdataReq2  data;

    public SaveJobWorkdataReq2 getData() {
        return data;
    }

    public void setData(SaveJobWorkdataReq2 data) {
        this.data = data;
    }
}
