package com.sevenrocks.taskapp.appModules.vendorreg

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.seekex.partyregistration.MyAdapter
import com.seekex.partyregistration.R
import com.seekex.partyregistration.models.apiResponse.DropListingDTO
import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.SpinnerAdapter_Payment
import com.seekex.partyregistration.vendorreg.adapters.SpinnerAdapter_city
import com.seekex.partyregistration.vendorreg.adapters.SpinnerAdapter_state
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.JobWorkedPartyGetResponse
import com.seekex.partyregistration.vendorreg.models.responses.VendorSaveResponse
import com.seekex.scheduleproject.AppConstants
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ValidationUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.basic_info.*
import kotlinx.android.synthetic.main.basic_info.view.*
import kotlinx.android.synthetic.main.header.view.*
import java.util.*


/**
 */
class EditBasicInfo : Fragment() {
    private lateinit var myAdapter: MyAdapter
    private var call_Counter = 0

    public var vendorActivity: VendorActivity? = null
    public var imageList = ArrayList<ImageDTO>()
    private lateinit var rootView: View

    var cityList: ArrayList<DropListingDTO> = java.util.ArrayList()

    var paymentId: String = "0"
    var operationid: String = "0"
    var stateId: String = "0"
    var cityId: String = "0"

    private lateinit var adapterPayment: SpinnerAdapter_Payment
    private lateinit var adapterState: SpinnerAdapter_state
    private lateinit var adapterCity: SpinnerAdapter_city


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.edt_basic_info, BasicActi, false)
        vendorActivity = activity as VendorActivity?
        myAdapter = MyAdapter(requireContext())

        rootView.header.text = "Edit Basic info"
        rootView.iv_save.visibility = View.VISIBLE
        rootView.back_icon.visibility = View.VISIBLE

        rootView.back_icon.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AllVendorList.instance,
                true
            )
        }
        rootView.iv_save.setOnClickListener {
            if (check()) {
                saveBasicDetails(getbasicDetailData())

            }
        }


        setAdapters()

        geJobPartyDetails()
        return rootView
    }


    private fun getbasicDetailData(): VendorRegDataReq {
        val reqData =
            VendorRegDataReq()
        val req =
            VendorRegReq()

        reqData.setService_name(ApiUtils.SAVE_PARTY)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)

        req.id = AllVendorList.dataModel.id
        req.name = edt_name.text.toString().trim()
        req.address = edt_address?.text.toString().trim()
        req.pincode = edt_pincode.text.toString().trim()
        req.mobile = edt_number.text.toString().trim()
        req.state_id = stateId
        req.city_id = cityId
        req.payment_term_id = paymentId
        req.year_of_experince = operationid

        req.gst_no = edt_gstnumber.text.toString().trim()
        req.pan_no = edt_pannumber.text.toString().trim()
        req.tin_no = edt_tinnumber.text.toString().trim()

        req.bank_name = edt_bankname.text.toString().trim()
        req.bank_ifsc_code = edt_ifsccode.text.toString().trim()
        req.bank_account = edt_accnumber.text.toString().trim()

        reqData.data = req

        return reqData
    }


    private fun geJobPartyDetails() {
        vendorActivity?.apiImp?.getJobLocationDetailById(
            getPartydetailrequestData(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as JobWorkedPartyGetResponse
                        rootView.edt_name.setText(ss.data.get(0).name)
                        rootView.edt_address.setText(ss.data.get(0).address)
                        rootView.edt_number.setText(ss.data.get(0).mobile)
                        rootView.edt_pincode.setText(ss.data.get(0).pincode)

                        rootView.edt_bankname.setText(ss.data.get(0).bank_name)
                        rootView.edt_ifsccode.setText(ss.data.get(0).bank_ifsc_code)
                        rootView.edt_accnumber.setText(ss.data.get(0).bank_account)
                        rootView.edt_gstnumber.setText(ss.data.get(0).gst_no)
                        rootView.edt_pannumber.setText(ss.data.get(0).pan_no)
                        rootView.edt_tinnumber.setText(ss.data.get(0).tin_no)

                        stateId = ss.data.get(0).state_id
                        cityId = ss.data.get(0).city_id
                        paymentId = ss.data.get(0).payment_term_id
                        operationid = ss.data.get(0).year_of_experince

                        var ss1 = DropListingDTO(ss.data.get(0).state_id)
                        var ss_city = DropListingDTO(ss.data.get(0).city_id)
                        var ss_paymenyid = DropListingDTO(ss.data.get(0).payment_term_id)

                        rootView.spin_state.isEnabled = true
                        rootView.spin_city.isEnabled = true
                        if (AllVendorList.stateListStat.contains(ss1)) {
                            var index = AllVendorList.stateListStat.indexOf(ss1)
                            rootView.spin_state.setSelection(index)
                        }
                        if (AllVendorList.paymenyListStat.contains(ss_paymenyid)) {
                            var index = AllVendorList.paymenyListStat.indexOf(ss_paymenyid)
                            rootView.spin_payment.setSelection(index)
                        }
                        rootView.spin_operation.setSelection(ss.data.get(0).year_of_experince.toInt())

                    }

                }

            })
    }

    private fun getPartydetailrequestData(): GetPartyDetailsForID {
        val reqData =
            GetPartyDetailsForID()
        val req =
            GetPartyDetailsForId2()

        reqData.setService_name(ApiUtils.GETPARTYLIST)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.page = 1;

        req.id = AllVendorList.dataModel.id
        req.name = ""
        req.mobile = ""


        reqData.conditions = req
        return reqData
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun saveBasicDetails(reqData: VendorRegDataReq) {

        vendorActivity?.apiImp?.saveBasicForm(reqData, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val ss = any as VendorSaveResponse

                    Toast.makeText(activity, "Record Updated", Toast.LENGTH_SHORT).show()
                    ExtraUtils.changeFragment(
                        vendorActivity!!.supportFragmentManager,
                        AllVendorList.instance,
                        true
                    )

                }

            }

        })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getCity(stateId: String) {
        cityList.clear()
        Log.e("TAG", "state id: " + this.stateId)
        val requestBase =
            getCityReq()
        requestBase.setService_name(ApiUtils.GET_CITY)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        requestBase.state_id = stateId
        vendorActivity?.apiImp?.getCity(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var aa =
                        DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select City"

                    cityList.add(aa)
                    var ss = any as DropListingResponse
                    Log.e("TAG", "onSuccess: " + ss.data.size)
                    cityList.addAll(ss.data)
                    adapterCity.notifyDataSetChanged()

                    var ss1 = DropListingDTO(AllVendorList.dataModel.city_id)


                    if (cityList.contains(ss1)) {
                        var index = cityList.indexOf(ss1)
                        rootView.spin_city.setSelection(index)


                    }
                }

            }

        })
    }


    private fun setAdapters() {
        adapterPayment =
            SpinnerAdapter_Payment(
                activity,
                AllVendorList.paymenyListStat
            )
        rootView.spin_payment.setAdapter(adapterPayment)


        adapterState =
            SpinnerAdapter_state(
                activity,
                AllVendorList.stateListStat
            )
        rootView.spin_state.setAdapter(adapterState)

        adapterCity =
            SpinnerAdapter_city(
                activity,
                cityList
            )

        rootView.spin_city.setAdapter(adapterCity)

        try {
            rootView.spin_state.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterState.getItem(position)!!
                    if (user.id != "0") {
                        rootView.spin_city.isEnabled = true
                        stateId = user.id
                        getCity(stateId)

                    }
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }

        try {
            rootView.spin_city.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterCity.getItem(position)!!

                    if (user.id != "0") {

                        cityId = user.id
                    }
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }


        try {
            rootView.spin_payment.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterPayment.getItem(position)!!
                    if (user.id != "0") {

                        paymentId = user.id
                    }
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
        try {
            rootView.spin_operation.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    operationid = adapterView!!.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }

    }

    private fun check(): Boolean {
        if (ValidationUtils.isEmpty(this.rootView.edt_name)) {
            vendorActivity?.dialogUtil?.showAlert(getString(R.string.err_name))
            return false
        }
        if (stateId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter State")
            return false
        }
        if (paymentId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter Payment Term")
            return false
        }
        if (operationid.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter Years of Operation")
            return false
        }
        if (cityId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter City")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_pincode)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Pincode")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_address)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Address")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_number)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Number")
            return false
        }
        if (!ValidationUtils.isValidMobileNo(this.rootView.edt_number.text.toString())) {
            vendorActivity?.dialogUtil?.showAlert("Enter correct number")
            return false
        }
        return true
    }


    companion object {

        @JvmStatic
        var alreadySelectedNames =
            VendorRegReq()

        @JvmStatic
        var partyId: String = ""


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = EditBasicInfo()
    }
}