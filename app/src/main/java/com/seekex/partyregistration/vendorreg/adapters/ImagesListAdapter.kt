package com.seekex.partyregistration.vendorreg.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seekex.partyregistration.databinding.BankAdapterBinding
import com.seekex.partyregistration.databinding.ImagesAdapterBinding
import com.seekex.partyregistration.databinding.JobtypeAdapterBinding
import com.seekex.partyregistration.databinding.LocationAdapterBinding
import com.seekex.partyregistration.vendorreg.models.request.GetPartyDetailsForID
import com.seekex.partyregistration.vendorreg.models.responses.JobImagesGetResponse2
import com.seekex.partyregistration.vendorreg.models.responses.JobLocationGetResponse2
import com.seekex.partyregistration.vendorreg.models.responses.JobTypeGetResponse2
import com.seekex.partyregistration.vendorreg.models.responses.JobWorkedPartyGetResponse2
import com.seekx.interfaces.AdapterListener

import java.util.ArrayList

class ImagesListAdapter(private val adapterListener: AdapterListener) :
    RecyclerView.Adapter<ImagesListAdapter.ViewHolder>() {

    private var items: ArrayList<JobImagesGetResponse2> = ArrayList()

    fun setDataValues(items: ArrayList<JobImagesGetResponse2>?) {
        this.items = items!!

        notifyDataSetChanged()
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ImagesAdapterBinding.inflate(inflater)


        binding.deleteImage.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }
        binding.ivImage.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 2)
        }
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: ImagesAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: JobImagesGetResponse2) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}