package com.seekex.partyregistration.vendorreg.vendor_jobtype

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.partyregistration.R
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.*
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.JobLocationGetResponse
import com.seekex.partyregistration.vendorreg.models.responses.JobLocationGetResponse2
import com.seekex.partyregistration.vendorreg.models.responses.JobTypeGetResponse2
import com.seekex.partyregistration.vendorreg.models.responses.JobtypeGetResponse
import com.seekex.scheduleproject.AppConstants
import com.seekx.interfaces.AdapterListener
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.vendorreg.AllVendorList
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.jobtype_layout.view.*
import kotlinx.android.synthetic.main.jobtype_list.view.*
import kotlinx.android.synthetic.main.location_list.view.*
import kotlinx.android.synthetic.main.vendor_list.view.recyclerView
import java.util.*


/**
 */
class AllJobtypeList : Fragment() {
    private lateinit var myAdapter: JobtypeListAdapter
    private var page = 1
    private var donHit = false
    var mydataList: ArrayList<JobTypeGetResponse2> = java.util.ArrayList()

    public var vendorActivity: VendorActivity? = null
    private lateinit var rootView: View


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.jobtype_list, BasicActi, false)
        vendorActivity = activity as VendorActivity?

        rootView.recyclerView.setLayoutManager(
            LinearLayoutManager(activity)
        )
        rootView.add_jobtype.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AddJobtypeInfo.instance,
                true
            )
        }
        setAdapter(rootView)
        page = 1
        getjobtypelist()
        headersettings()



        return rootView
    }

    private fun headersettings() {
        rootView.header.text = "JobType List"
        rootView.back_icon.visibility = View.VISIBLE
        rootView.back_icon.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AllVendorList.instance,
                true
            )

        }
    }

    override fun onResume() {
        super.onResume()
    }

    private fun setAdapter(rootView: View) {
        myAdapter = JobtypeListAdapter(object : AdapterListener {
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                jobtypeModel = any1 as JobTypeGetResponse2

                when (i) {
                    2 -> {
                        ExtraUtils.changeFragment(
                            vendorActivity!!.supportFragmentManager,
                            EditJobtypeInfo.instance,
                            true
                        )

                    }
                    3 -> {
                        deleteLocation(jobtypeModel)

                        //hit delete api
                    }

                }

            }

        })
        rootView.recyclerView!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.recyclerView!!.adapter = myAdapter

        rootView.recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !donHit) {
                    Log.v("addOnScrollListener", "false")
                    getjobtypelist()
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        myAdapter!!.notifyDataSetChanged()
    }

    private fun deleteLocationRequestData(): DeleteDTO {
        val reqData =
            DeleteDTO()


        reqData.setService_name(ApiUtils.JOBTYPEDELETE)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.id = jobtypeModel.id

        return reqData
    }

    private fun deleteLocation(locationModel: JobTypeGetResponse2) {
        vendorActivity?.apiImp?.deleteLocation(
            deleteLocationRequestData(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {

                        mydataList.remove(locationModel)
                        Toast.makeText(activity, "Record Deleted", Toast.LENGTH_SHORT).show()

                        myAdapter.notifyDataSetChanged()
                    }
                }

            })
    }

    private fun getjobtypelist() {
        vendorActivity?.apiImp?.getJobtypeList(
            getjobtypeListData(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as JobtypeGetResponse
                        val datalist = ss.data

                        if (datalist.isEmpty()) {
                            donHit = true
                        } else {
                            mydataList.addAll(datalist)
                            myAdapter!!.setDataValues(mydataList)
                            myAdapter!!.notifyDataSetChanged()
                            page++
                        }
                    }
                }

            })
    }

    private fun getjobtypeListData(): GetalljobtypeList {
        val reqData =
            GetalljobtypeList()
        val req=
            GetAllJobtypeList2()

        reqData.setService_name(ApiUtils.JOBTYPEGET)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.page = page;
        reqData.party_id = AllVendorList.dataModel.id


        req.id = ""
        reqData.conditions = req
        return reqData
    }

    companion object {

        @JvmStatic
        var jobtypeModel = JobTypeGetResponse2()

        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AllJobtypeList()
    }
}