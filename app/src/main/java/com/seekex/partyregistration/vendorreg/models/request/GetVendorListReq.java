package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class GetVendorListReq extends RequestBase {

    private GetVendorListReq2  conditions;

    public GetVendorListReq2 getConditions() {
        return conditions;
    }

    public void setConditions(GetVendorListReq2 conditions) {
        this.conditions = conditions;
    }
}
