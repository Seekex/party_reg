package com.sevenrocks.taskapp.appModules.vendorreg

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.partyregistration.R
import com.seekex.partyregistration.models.apiResponse.DropListingDTO
import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.retrofitclasses.RequestBase
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.*
import com.seekex.partyregistration.vendorreg.images.AllImagesList
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.JobWorkedPartyGetResponse
import com.seekex.partyregistration.vendorreg.models.responses.JobWorkedPartyGetResponse2
import com.seekex.partyregistration.vendorreg.vendor_jobtype.AllJobtypeList
import com.seekex.partyregistration.vendorreg.vendor_loc.AllLocationList
import com.seekex.scheduleproject.AppConstants
import com.seekx.interfaces.AdapterListener
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.vendor_list.view.*
import java.util.*


/**
 */
class AllVendorList : Fragment() {
    private lateinit var myAdapter: VendorListAdapter
    private var page = 1
    private var donHit = false
    var mydataList: ArrayList<JobWorkedPartyGetResponse2> = java.util.ArrayList()
    var joblist: ArrayList<DropListingDTO> = java.util.ArrayList()
    var paymentlist: ArrayList<DropListingDTO> = java.util.ArrayList()
    var stateList: ArrayList<DropListingDTO> = java.util.ArrayList()
    var cityList: ArrayList<DropListingDTO> = java.util.ArrayList()
    public var vendorActivity: VendorActivity? = null
    private lateinit var rootView: View


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.vendor_list, BasicActi, false)
        vendorActivity = activity as VendorActivity?

        rootView.recyclerView.setLayoutManager(
            LinearLayoutManager(activity)
        )

        setAdapter(rootView)

        getStates()
        getPaymentTerms()
        getJobtypes()

        page = 1
        getallPartyList()
        headersettings()



        return rootView
    }

    private fun headersettings() {
        rootView.header.text = "Vendor List"
        rootView.back_icon.visibility = View.VISIBLE
        rootView.back_icon.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                VendorMenuFragment.instance,
                true
            )

        }
    }

    override fun onResume() {
        super.onResume()
        page = 1
    }

    private fun getAllvendorReqdata(): GetAllVendorList {
        val reqData =
            GetAllVendorList()
        val req =
            GetAllVendorList2()

        reqData.setService_name(ApiUtils.GETPARTYLIST)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.page = page;
        req.id = ""
        req.name = ""
        req.mobile = ""


        reqData.conditions = req
        return reqData
    }

    private fun setAdapter(rootView: View) {
        myAdapter = VendorListAdapter(object : AdapterListener {
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                dataModel = any1 as JobWorkedPartyGetResponse2

                when (i) {
                    1 -> {


                    }
                    2 -> {
                        ExtraUtils.changeFragment(
                            vendorActivity!!.supportFragmentManager,
                            EditBasicInfo.instance,
                            true
                        )
                    }
                    3 -> {
                        ExtraUtils.changeFragment(
                            vendorActivity!!.supportFragmentManager,
                            AllImagesList.instance,
                            true
                        )
                    }
                    4 -> {
                        ExtraUtils.changeFragment(
                            vendorActivity!!.supportFragmentManager,
                            AllJobtypeList.instance,
                            true
                        )
                    }
                    5 -> {
                        ExtraUtils.changeFragment(
                            vendorActivity!!.supportFragmentManager,
                            AllLocationList.instance,
                            true
                        )
                    }
                    6 -> {
                      deleteParty(dataModel)
                    }
                }
            }

        })
        rootView.recyclerView!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.recyclerView!!.adapter = myAdapter

        rootView.recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !donHit) {
                    Log.v("addOnScrollListener", "false")
                    getallPartyList()
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        myAdapter!!.notifyDataSetChanged()
    }
    private fun deletePartyRequestData(): DeleteDTO {
        val reqData =
            DeleteDTO()


        reqData.setService_name(ApiUtils.DELETEPARTY)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        return reqData
    }

    private fun deleteParty(dataModel: JobWorkedPartyGetResponse2) {
        vendorActivity?.apiImp?.deleteParty(
            deletePartyRequestData(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        mydataList.remove(dataModel)
                        Toast.makeText(activity, "Record Deleted", Toast.LENGTH_SHORT).show()
                        myAdapter.notifyDataSetChanged()
                    }
                }

            })
    }

    private fun getallPartyList() {
        vendorActivity?.apiImp?.getAllJobList(
            getAllvendorReqdata(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as JobWorkedPartyGetResponse
                        val datalist = ss.data

                        if (datalist.isEmpty()) {
                            donHit = true
                        } else {
                            mydataList.addAll(datalist)
                            myAdapter!!.setDataValues(mydataList)
                            myAdapter!!.notifyDataSetChanged()
                            page++
                        }
                    }

                }

            })


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getPaymentTerms() {
        paymenyListStat.clear()
        val requestBase = RequestBase()
        requestBase.setService_name(ApiUtils.GET_PAYMENT_TERMS)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        vendorActivity?.apiImp?.getPaymentTerms(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var aa =
                        DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select Term"

                    paymentlist.add(aa)
                    var ss = any as DropListingResponse
                    paymentlist.addAll(ss.data)
                    paymenyListStat.addAll(paymentlist)

                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getCity(stateId: String) {
        cityList.clear()
        val requestBase =
            getCityReq()
        requestBase.setService_name(ApiUtils.GET_CITY)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        requestBase.state_id = stateId
        vendorActivity?.apiImp?.getCity(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var aa =
                        DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select City"

                    cityList.add(aa)
                    var ss = any as DropListingResponse
                    Log.e("TAG", "onSuccess: " + ss.data.size)
                    cityList.addAll(ss.data)
                    citylistStat.addAll(ss.data)


                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getStates() {
        stateListStat.clear()
        val requestBase = RequestBase()
        requestBase.setService_name(ApiUtils.GET_STATES)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        vendorActivity?.apiImp?.getStates(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var aa = DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select State"

                    stateList.add(aa)
                    var ss = any as DropListingResponse
                    stateList.addAll(ss.data)
                    stateListStat.addAll(stateList)

                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getJobtypes() {
        jobtypeListstat.clear()
        val requestBase = RequestBase()
        requestBase.setService_name(ApiUtils.GET_JOB_TYPES)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        vendorActivity?.apiImp?.getListing(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as DropListingResponse
                    var aa =
                        DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select Jobtype"

                    joblist.add(aa)

                    joblist.addAll(ss.data)
                    jobtypeListstat.addAll(joblist)

                }

            }

        })
    }


    companion object {
        @JvmStatic
        val citylistStat = java.util.ArrayList<DropListingDTO>()

        @JvmStatic
        var stateListStat =
            java.util.ArrayList<DropListingDTO>()

        @JvmStatic
        var paymenyListStat =
            java.util.ArrayList<DropListingDTO>()

        @JvmStatic
        var jobtypeListstat =
            java.util.ArrayList<DropListingDTO>()

        @JvmStatic
        var dataModel = JobWorkedPartyGetResponse2()

        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AllVendorList()
    }
}