package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class GetallLocationList extends RequestBase {

    private GetAllLocationList2  conditions;
    private Integer  page;
    private String  party_id;

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public GetAllLocationList2 getConditions() {
        return conditions;
    }

    public void setConditions(GetAllLocationList2 conditions) {
        this.conditions = conditions;
    }
}
