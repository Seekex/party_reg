package com.seekex.partyregistration.vendorreg

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.seekex.partyregistration.R
import com.seekex.partyregistration.activities.LoginActivity
import com.seekex.partyregistration.interfaces.ImageSelectionCallback
import com.seekex.scheduleproject.AppConstants
import com.seekex.scheduleproject.Preferences
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp

class VendorActivity : AppCompatActivity() {
    var uid = 0
    var pref: Preferences? = null
    var dialogUtil: DialogUtils? = null
    var apiImp: ApiImp? = null
    private lateinit var imageSelectionCallback: ImageSelectionCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.framelayout_container)
        //        Objects.requireNonNull(getSupportActionBar()).hide();
        init()
    }

    private fun init() {
        pref = Preferences(this)
        dialogUtil = DialogUtils(this)
        apiImp = ApiImp(this)
        ExtraUtils.changeFragment(supportFragmentManager, VendorMenuFragment.instance, true)
    }

    fun logoutt() {
        pref?.set(AppConstants.uid, "")
        pref?.setBoolean(AppConstants.isLogin, false)

        val i = Intent(this, LoginActivity::class.java)
// set the new task and clear flags
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)

    }


    override fun onBackPressed() {
//        super.onBackPressed();
        //  FragmentManager supportFragmentManager=getSupportFragmentManager();
        //  Fragment f=supportFragmentManager.findFragmentById(R.id.framcontainer);

        /*  if(f instanceof InventoryFor)
            ExtraUtils.changeFragment(getSupportFragmentManager(), InventoryMenuFragment.getInstance(),true);
        else*/
//            ExtraUtils.handleFragment(getSupportFragmentManager(),this);
//        finish()
    }
}