package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class VendorSaveImageReq extends RequestBase {

    private String  file;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
