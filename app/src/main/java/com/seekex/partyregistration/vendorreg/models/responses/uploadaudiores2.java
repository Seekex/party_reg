package com.seekex.partyregistration.vendorreg.models.responses;

import com.google.gson.annotations.SerializedName;

public class uploadaudiores2 {
    @SerializedName("path")
        private String path;

    @SerializedName("filename")
        private String filename;

    @SerializedName("file")
        private String file;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
