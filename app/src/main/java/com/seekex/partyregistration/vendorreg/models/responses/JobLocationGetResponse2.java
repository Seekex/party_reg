
package com.seekex.partyregistration.vendorreg.models.responses;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

import java.util.Objects;

public class JobLocationGetResponse2 extends ResponseBase {



    @SerializedName("id")
    private String id;
    @SerializedName("party_id")
    private String party_id;
    @SerializedName("parent_location_id")
    private String parent_location_id;
    @SerializedName("name")
    private String name;
    @SerializedName("state_id")
    private String state_id;
    @SerializedName("city_id")
    private String city_id;
    @SerializedName("pincode")
    private String pincode;

    @SerializedName("address")
    private String address;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("state")
    private String state;
    @SerializedName("city")
    private String city;


    public String getMobileText() {
        return "Mobile: "+mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getParent_location_id() {
        return parent_location_id;
    }

    public void setParent_location_id(String parent_location_id) {
        this.parent_location_id = parent_location_id;
    }

    public String getName() {
        return name;
    }
    public String getNameText() {
        return "Name: "+name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }
    public String getPincodeText() {
        return "Pincode: "+pincode;
    }
    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
    public String getAddressText() {
        return "Address: "+address;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getStateText() {
        return "State: "+state;
    }
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    public String getCityText() {
        return "City: "+city;
    }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobLocationGetResponse2 that = (JobLocationGetResponse2) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

