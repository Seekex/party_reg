
package com.seekex.partyregistration.vendorreg.models.responses;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

public class ImageSaveResponse extends ResponseBase {
    @SerializedName("party_id")
    private String party_id;

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

