package com.seekex.partyregistration.vendorreg.vendor_jobtype

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.seekex.partyregistration.R
import com.seekex.partyregistration.models.apiResponse.DropListingDTO
import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.*
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.*
import com.seekex.scheduleproject.AppConstants
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ValidationUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.vendorreg.AllVendorList
import com.sevenrocks.taskapp.appModules.vendorreg.VendorRegistration
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.jobtype_layout.*
import kotlinx.android.synthetic.main.jobtype_layout.view.*
import kotlinx.android.synthetic.main.location_layout.*
import kotlinx.android.synthetic.main.location_layout.view.*
import java.util.*


/**
 */
class EditJobtypeInfo : Fragment() {

    public var vendorActivity: VendorActivity? = null
    private lateinit var rootView: View

    var jobtypelist: ArrayList<DropListingDTO> = java.util.ArrayList()

    var type_id: String = "0"

    private lateinit var adapterJoblist: SpinnerAdapter_joblist


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.edt_jobtype, BasicActi, false)
        vendorActivity = activity as VendorActivity?


        headerSettings()

        setAdapters()

        getJobtypeDetails()
        return rootView
    }

    private fun headerSettings() {

        rootView.header.text = "Edit Location"
        rootView.iv_save.visibility = View.VISIBLE
        rootView.back_icon.visibility = View.VISIBLE

        rootView.back_icon.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AllJobtypeList.instance,
                true
            )
        }
        rootView.iv_save.setOnClickListener {
            if (checkLoc()) {
                updateJobworkValues()

            }

        }
    }


    private fun updateJobworkValues() {
        val reqData =
            SaveJobWorkdataReq()
        val req =
            SaveJobWorkdataReq2()

        reqData.setService_name(ApiUtils.JOBTYPESAVE)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)

        req.party_id = AllVendorList.dataModel.id
        req.capacity = edt_capacity.text.toString().trim()
        req.job_type_id = type_id
        req.id = AllJobtypeList.jobtypeModel.id


        reqData.data = req

        vendorActivity?.apiImp?.saveJobtypeData(reqData, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    ExtraUtils.changeFragment(
                        vendorActivity!!.supportFragmentManager,
                        AllJobtypeList.instance,
                        true
                    )
                }

            }

        })


    }


    private fun getjobtypeListData(): GetalljobtypeList {
        val reqData =
            GetalljobtypeList()
        val req =
            GetAllJobtypeList2()

        reqData.setService_name(ApiUtils.JOBTYPEGET)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.page = 1;
        reqData.party_id = AllVendorList.dataModel.id


        req.id = AllJobtypeList.jobtypeModel.id
        reqData.conditions = req
        return reqData
    }


    private fun getJobtypeDetails() {
        vendorActivity?.apiImp?.getJobtypedetailById(
            getjobtypeListData(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as JobtypeGetResponse
                        Log.e("TAG", "onSuccess: "+ss.data.get(0).job_type_id )
                        rootView.edt_capacity.setText(ss.data.get(0).capacity)
type_id= ss.data.get(0).job_type_id
                        var ss1 = DropListingDTO(ss.data.get(0).job_type_id)

                        if (AllVendorList.jobtypeListstat.contains(ss1)) {
                            var index =
                                AllVendorList.jobtypeListstat.indexOf(ss1)
                            rootView.spin_type.setSelection(index)
                        }
                    }

                }

            })
    }


    private fun setAdapters() {



        adapterJoblist =
            SpinnerAdapter_joblist(
                activity,
                AllVendorList.jobtypeListstat
            )

        rootView.spin_type.setAdapter(adapterJoblist)

        try {
            rootView.spin_type.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterJoblist.getItem(position)!!
                    if (user.id != "0") {
                        type_id = user.id


                    }
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }

    }


    private fun checkLoc(): Boolean {

        if (type_id.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter Jobtype")
            return false
        }

        if (ValidationUtils.isEmpty(this.rootView.edt_capacity)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Capacity")
            return false
        }

        return true
    }


    companion object {


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = EditJobtypeInfo()
    }
}