package com.sevenrocks.taskapp.appModules.vendorreg

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.seekex.partyregistration.GPSTracker
import com.seekex.partyregistration.MyAdapter
import com.seekex.partyregistration.R
import com.seekex.partyregistration.databinding.ImageItemBinding
import com.seekex.partyregistration.interfaces.SelectedMasterCallback
import com.seekex.partyregistration.models.apiResponse.DropListingDTO
import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.retrofitclasses.RequestBase
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.SpinnerAdapter_Payment
import com.seekex.partyregistration.vendorreg.adapters.SpinnerAdapter_city
import com.seekex.partyregistration.vendorreg.adapters.SpinnerAdapter_joblist
import com.seekex.partyregistration.vendorreg.adapters.SpinnerAdapter_state
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.JobWorkedPartyGetResponse
import com.seekex.partyregistration.vendorreg.models.responses.VendorSaveResponse
import com.seekex.partyregistration.vendorreg.models.responses.uploadaudiores
import com.seekex.partyregistration.vendorreg.vendor_loc.ImagesDTO
import com.seekex.scheduleproject.AppConstants
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ValidationUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.addimage_layout.*
import kotlinx.android.synthetic.main.addimage_layout.view.*
import kotlinx.android.synthetic.main.basic_info.*
import kotlinx.android.synthetic.main.basic_info.view.*
import kotlinx.android.synthetic.main.choose_adapter.*
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.image_view.*
import kotlinx.android.synthetic.main.jobtype_layout.*
import kotlinx.android.synthetic.main.jobtype_layout.view.*
import kotlinx.android.synthetic.main.location_layout.*
import kotlinx.android.synthetic.main.location_layout.view.*
import kotlinx.android.synthetic.main.vendor_reg.view.*
import java.util.*
import kotlin.collections.ArrayList


/**
 */
class VendorRegistration : Fragment(), LocationListener {
    private var ss_city: DropListingDTO? =null
    private var citynameLoc: String? = ""
    private lateinit var myAdapter: MyAdapter
    private var currentView = 0
    private var call_Counter = 0
    protected var locationManager: LocationManager? = null
    protected var locationListener: LocationListener? = null
    public var vendorActivity: VendorActivity? = null
    public var imageList = ArrayList<ImagesDTO>()
    private lateinit var rootView: View
    var joblist: ArrayList<DropListingDTO> = java.util.ArrayList()
    var paymentlist: ArrayList<DropListingDTO> = java.util.ArrayList()
    var stateList: ArrayList<DropListingDTO> = java.util.ArrayList()
    var cityList: ArrayList<DropListingDTO> = java.util.ArrayList()

    var paymentId: String = "0"
    var operationid: String = "0"
    var joblistid: String = "0"
    var stateId: String = "0"
    var cityId: String = "0"

    var stateIdLoc: String = "0"
    var cityIdLoc: String = "0"

    private lateinit var adapterJoblist: SpinnerAdapter_joblist
    private lateinit var adapterPayment: SpinnerAdapter_Payment
    private lateinit var adapterState: SpinnerAdapter_state
    private lateinit var adapterCity: SpinnerAdapter_city
    private lateinit var adapterCityLoc: SpinnerAdapter_city

    var gps: GPSTracker? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.vendor_reg, BasicActi, false)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())



        vendorActivity = activity as VendorActivity?
        myAdapter = MyAdapter(requireContext())
        getStates()
        rootView.header.text = "Vendor Registration"
        rootView.iv_save.visibility = View.VISIBLE
        rootView.back_icon.visibility = View.VISIBLE
        rootView.back_icon.setOnClickListener {
            managerViews(true)
        }
        rootView.iv_save.setOnClickListener {
            managerViews(false)
        }
        managerVisibility(
            rootView.ll_basics,
            rootView.ll_addimages,
            rootView.ll_addjobtype,
            rootView.add_locationDetails
        )

        setListeners()


        setAdapters()
        getJobtypes()
        getPaymentTerms()



        rootView.skip.setOnClickListener {

            skipManage()
        }

        return rootView
    }

    private fun managerVisibility(first: View, second: View, third: View, forth: View) {
        first.visibility = View.VISIBLE
        second.visibility = View.GONE
        third.visibility = View.GONE
        forth.visibility = View.GONE
    }

    private fun managerViews(isBack: Boolean) {
        if (isBack) {
            when (currentView) {
                0 -> {
                    ExtraUtils.changeFragment(
                        vendorActivity!!.supportFragmentManager,
                        VendorMenuFragment.instance,
                        true
                    )

                }
                1 -> {
                    currentView = 0
                    manageVisibility()
                    rootView.back_icon.visibility = View.VISIBLE
                    rootView.logout.visibility = View.GONE
                    rootView.skip.visibility = View.GONE


                }
                2 -> {
                    currentView = 1
                    manageVisibility()
                }
                3 -> {
                    currentView = 2
                    manageVisibility()
                }
            }
        } else {
            when (currentView) {
                0 -> {
                    if (check()) {
                        updateValues()
                    }
                }
                1 -> {
                    if (imageList.size > 0) {
                        uploadImages()
                    } else {
                        Toast.makeText(activity, "no Image added", Toast.LENGTH_SHORT).show()
                    }


                }
                2 -> {
                    if (joblistid.equals("0")) {
                        vendorActivity?.dialogUtil?.showAlert("Select JobType")
                        return
                    }
                    if (edt_capacity.text.toString().length == 0) {
                        vendorActivity?.dialogUtil?.showAlert("Enter Capacity")
                        return
                    }
                    updateJobworkValues()
                }
                3 -> {

                    if (checkLoc()) {
                        updateLocations()

                    }
                }
            }
        }

    }

    private fun updateLocations() {
        val reqData =
            SaveLocationdataReq()
        val req =
            SaveLocationdataReq2()

        reqData.setService_name(ApiUtils.SAVELOCATION)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)


        req.id = ""
        req.party_id = partyId
        req.name = edt_locname.text.toString().trim()
        req.address = edt_locaddress.text.toString().trim()
        req.mobile = edt_locnumber.text.toString().trim()
        req.pincode = edt_locpincode.text.toString().trim()
        req.state_id = stateIdLoc
        req.city_id = cityIdLoc
        req.is_active = true


        reqData.data = req

        vendorActivity?.apiImp?.saveLocation(reqData, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    Log.e("TAG", "onSuccess: ")
                    Toast.makeText(activity, "Data saved", Toast.LENGTH_SHORT).show()
                    ExtraUtils.changeFragment(
                        vendorActivity!!.supportFragmentManager,
                        VendorMenuFragment.instance,
                        true
                    )

//                    vendorActivity!!.finish()

                }

            }

        })


    }

    private fun skipManage() {

        when (currentView) {
            0 -> {
                if (check()) {
                    updateValues()
                }
            }
            1 -> {

                currentView = 2
                manageVisibility()

            }
            2 -> {
                currentView = 3
                manageVisibility()
                geJobLocationDetails()

            }
            3 -> {
                Toast.makeText(activity, "Data saved", Toast.LENGTH_SHORT).show()

                ExtraUtils.changeFragment(
                    vendorActivity!!.supportFragmentManager,
                    VendorMenuFragment.instance,
                    true
                )

//                vendorActivity?.finish()

            }
        }


    }

    private fun updateValues() {
        saveBasicDetails(getbasicDetailData())
    }

    private fun getbasicDetailData(): VendorRegDataReq {
        val reqData =
            VendorRegDataReq()
        val req =
            VendorRegReq()

        reqData.setService_name(ApiUtils.SAVE_PARTY)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)

        req.id = ""
        req.name = edt_name.text.toString().trim()
        req.address = edt_address?.text.toString().trim()
        req.pincode = edt_pincode.text.toString().trim()
        req.mobile = edt_number.text.toString().trim()
        req.state_id = stateId
        req.city_id = cityId
        req.payment_term_id = paymentId
        req.year_of_experince = operationid

        req.gst_no = edt_gstnumber.text.toString().trim()
        req.pan_no = edt_pannumber.text.toString().trim()
        req.tin_no = edt_tinnumber.text.toString().trim()

        req.bank_name = edt_bankname.text.toString().trim()
        req.bank_ifsc_code = edt_ifsccode.text.toString().trim()
        req.bank_account = edt_accnumber.text.toString().trim()

        reqData.data = req

        return reqData
    }

    private fun updateJobworkValues() {
        val reqData =
            SaveJobWorkdataReq()
        val req =
            SaveJobWorkdataReq2()

        reqData.setService_name(ApiUtils.JOBTYPESAVE)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)

        req.party_id = partyId
        req.capacity = edt_capacity.text.toString().trim()
        req.job_type_id = joblistid


        reqData.data = req

        vendorActivity?.apiImp?.saveJobtypeData(reqData, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    currentView = 3
                    manageVisibility()
                    geJobLocationDetails()


                }

            }

        })


    }

    private fun geJobLocationDetails() {
        vendorActivity?.apiImp?.getJobLocationDetailById(
            getPartydetailrequestData(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as JobWorkedPartyGetResponse

                        Log.e("TAG", "onSuccess: "+ss.data.size)
                        Log.e("TAG", "onSuccess: "+ss.data.get(0).name)
                        Log.e("TAG", "onSuccess: "+ss.data.get(0).state_id)
                        Log.e("TAG", "onSuccess: "+ss.data.get(0).city_id)
                        rootView.edt_locname.setText(ss.data.get(0).name)
                        rootView.edt_locaddress.setText(ss.data.get(0).address)
                        rootView.edt_locnumber.setText(ss.data.get(0).mobile)
                        rootView.edt_locpincode.setText(ss.data.get(0).pincode)

                        var ss1 = DropListingDTO(ss.data.get(0).state_id)
                         ss_city = DropListingDTO(ss.data.get(0).city_id)
                        stateIdLoc = ss.data.get(0).state_id
                        cityIdLoc = ss.data.get(0).city_id
                        if (stateList.contains(ss1)) {
                            var index = stateList.indexOf(ss1)
                            rootView.spin_locstate.setSelection(index)
                        }
                    }

                }

            })
    }

    private fun getPartydetailrequestData(): GetPartyDetailsForID {
        val reqData =
            GetPartyDetailsForID()
        val req =
            GetPartyDetailsForId2()

        reqData.setService_name(ApiUtils.GETPARTYLIST)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.page = 1;

        req.id = partyId
        req.name = edt_name.text.toString().trim()
        req.mobile = edt_number.text.toString().trim()


        reqData.conditions = req
        return reqData
    }


    private fun uploadImages() {


        vendorActivity?.apiImp?.hitApi(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e("TAG", "onSuccess: filepath=")

                if (status) {
                    var ss = any as uploadaudiores
                    var filepath = ss.data.path + ss.data.filename
                    Log.e("TAG", "onSuccess: filepath=$filepath")
                    vendorActivity?.apiImp?.hitDataApi(
                        partyId,
                        imageList,
                        filepath,
                        object : ApiCallBack {
                            override fun onSuccess(status: Boolean, any: Any) {
                                if (status) {
                                    Log.e("TAG", "onSuccess: " + imageList.size)
                                    Log.e("TAG", "callcounter: " + call_Counter)
                                    call_Counter++

                                    if (call_Counter < imageList.size) {

                                        uploadImages()

                                    } else {
                                        currentView = 2
                                        manageVisibility()
                                    }
                                }
                            }

                        })
                }


            }

        })


    }


    private fun manageVisibility() {
        manageViews()
        when (currentView) {
            1 -> {
                managerVisibility(
                    rootView.ll_addimages,
                    rootView.ll_addjobtype,
                    rootView.ll_basics,
                    rootView.add_locationDetails
                )
            }
            2 -> {
                managerVisibility(
                    rootView.ll_addjobtype,
                    rootView.ll_addimages,
                    rootView.ll_basics,
                    rootView.add_locationDetails
                )
            }
            3 -> {
                managerVisibility(
                    rootView.add_locationDetails,
                    rootView.ll_addimages,
                    rootView.ll_basics,
                    rootView.ll_addjobtype
                )
            }
            0 -> {
                managerVisibility(
                    rootView.ll_basics,
                    rootView.add_locationDetails,
                    rootView.ll_addimages,
                    rootView.ll_addjobtype
                )
            }
        }
    }

    private fun manageViews() {

        when (currentView) {
            0 -> {
                this.rootView.vOne.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vTwo.setBackgroundColor(resources.getColor(R.color.darkWhite))
                this.rootView.vThree.setBackgroundColor(resources.getColor(R.color.darkWhite))
                this.rootView.vFour.setBackgroundColor(resources.getColor(R.color.darkWhite))
            }
            1 -> {
                this.rootView.vOne.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vTwo.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vThree.setBackgroundColor(resources.getColor(R.color.darkWhite))
                this.rootView.vFour.setBackgroundColor(resources.getColor(R.color.darkWhite))
            }
            2 -> {
                this.rootView.vOne.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vTwo.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vThree.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vFour.setBackgroundColor(resources.getColor(R.color.darkWhite))

            }
            3 -> {
                this.rootView.vOne.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vTwo.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vThree.setBackgroundColor(resources.getColor(R.color.colorOrange))
                this.rootView.vFour.setBackgroundColor(resources.getColor(R.color.colorOrange))
            }
        }


    }


    private fun getGPSLocation() {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        } else {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    // Got last known location. In some rare situations this can be null.

                    try {

                        getlocation(location!!.latitude, location!!.longitude)
                    } catch (e: Exception) {
                    }
                }
        }

    }

    private fun getlocation(latitude: Double, longitude: Double) {
        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(activity, Locale.getDefault())

        addresses = geocoder.getFromLocation(
            latitude,
            longitude,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5


        val address =
            addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        citynameLoc = addresses[0].locality
        val state = addresses[0].adminArea
        val country = addresses[0].countryName
        val postalCode = addresses[0].postalCode
        val knownName = addresses[0].featureName // Only if available else return NULL
        Log.e("TAG", "getlocation: " + address)
        Log.e("TAG", "getlocation: " + stateList.size)

        rootView.edt_address.setText(address)
        rootView.edt_pincode.setText(postalCode)

        var ss1 = DropListingDTO(getIdByName(state, stateList))

        if (stateList.contains(ss1)) {
            Log.e("TAG", "incontain: " + state)
            var index = stateList.indexOf(ss1)
            Log.e("TAG", "index: " + index)

            rootView.spin_state.setSelection(index)
        }
        Log.e("TAG", "getlocation: citylist" + cityList.size)



        rootView.edt_pincode.setText(postalCode)

    }

    private fun getIdByName(state: String?, list: ArrayList<DropListingDTO>): String {

        for (item in list) {
            if (item.name.toLowerCase().equals(state!!.toLowerCase())) {
                Log.e("TAG", "getIdByName: " + item.id)
                return item.id
            }
        }

        return ""
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun saveBasicDetails(reqData: VendorRegDataReq) {

        vendorActivity?.apiImp?.saveBasicForm(reqData, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val ss = any as VendorSaveResponse
                    partyId = ss.party_id
                    rootView.back_icon.visibility = View.VISIBLE
                    rootView.skip.visibility = View.VISIBLE
                    rootView.logout.visibility = View.GONE
                    currentView = 1
                    manageVisibility()
                }

            }

        })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getPaymentTerms() {
        val requestBase = RequestBase()
        requestBase.setService_name(ApiUtils.GET_PAYMENT_TERMS)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        vendorActivity?.apiImp?.getPaymentTerms(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var aa =
                        DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select Term"

                    paymentlist.add(aa)
                    var ss = any as DropListingResponse
                    paymentlist.addAll(ss.data)
                    adapterPayment.notifyDataSetChanged()

                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getCity(stateId: String) {
        cityList.clear()
        Log.e("TAG", "state id: " + stateId)
        Log.e("TAG", "cityIdLoc: " + cityIdLoc)
        val requestBase =
            getCityReq()
        requestBase.setService_name(ApiUtils.GET_CITY)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        requestBase.state_id = stateId
        vendorActivity?.apiImp?.getCity(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var aa =
                        DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select City"

                    cityList.add(aa)
                    var ss = any as DropListingResponse
                    Log.e("TAG", "onSuccess: " + ss.data.size)
                    cityList.addAll(ss.data)
                    rootView.spin_loccity.isEnabled = true

                    adapterCity.notifyDataSetChanged()
                    adapterCityLoc.notifyDataSetChanged()

                    if (cityList.contains(ss_city)) {
                        Log.e("TAG", "cityIdLoc: "+ cityIdLoc)
                        var index = cityList.indexOf(ss_city)
                        Log.e("TAG", "index: "+ index)

                        rootView.spin_loccity.setSelection(index)
                    }
                    if (!citynameLoc.equals("")) {
                        var _city = DropListingDTO(getIdByName(citynameLoc, cityList))
                        if (cityList.contains(_city)) {
                            var index = cityList.indexOf(_city)
                            rootView.spin_city.setSelection(index)
                        }

                    }
                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getStates() {
        val requestBase = RequestBase()
        requestBase.setService_name(ApiUtils.GET_STATES)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        vendorActivity?.apiImp?.getStates(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var aa = DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select State"

                    stateList.add(aa)
                    var ss = any as DropListingResponse
                    stateList.addAll(ss.data)
                    adapterState.notifyDataSetChanged()
                    chechLatLngandGetResult()
                }

            }

        })
    }

    private fun chechLatLngandGetResult() {
        if (VendorMenuFragment.latitude == 0.0 && VendorMenuFragment.longitude == 0.0) {
            getGPSLocation()
        } else {
            getlocation(VendorMenuFragment.latitude, VendorMenuFragment.longitude)
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getJobtypes() {

        val requestBase = RequestBase()
        requestBase.setService_name(ApiUtils.GET_JOB_TYPES)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        vendorActivity?.apiImp?.getListing(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as DropListingResponse
                    var aa =
                        DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select Jobtype"

                    joblist.add(aa)

                    joblist.addAll(ss.data)
                    adapterJoblist.notifyDataSetChanged()

                }

            }

        })
    }

//    private fun getLocation() {
//        locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
//        if ((ContextCompat.checkSelfPermission(
//                requireActivity(),
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED)
//        ) {
//            ActivityCompat.requestPermissions(
//                requireActivity(),
//                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
//                locationPermissionCode
//            )
//        }
//    }
//
//
//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        when (requestCode) {
//            1 -> {
//
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.isNotEmpty()
//                    && grantResults[0] === PackageManager.PERMISSION_GRANTED
//                ) {
//
//                    gps = GPSTracker(activity, activity)
//
//                    // Check if GPS enabled
//                    if (gps!!.canGetLocation()) {
//                        val latitude = gps!!.latitude
//                        val longitude = gps!!.longitude
//
//                        // \n is for new line
//                        Toast.makeText(
//                            activity,
//                            "Your Location is  this - \nLat: $latitude\nLong: $longitude",
//                            Toast.LENGTH_LONG
//                        ).show()
//
//                    } else {
//                        // Can't get location.
//                        // GPS or network is not enabled.
//                        // Ask user to enable GPS/network in settings.
//                        gps!!.showSettingsAlert()
//                    }
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    Toast.makeText(activity, "You need to grant permission", Toast.LENGTH_SHORT)
//                        .show()
//                }
//                return
//            }
//
//            2 -> {
//                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // Check if GPS enabled
//                    if (gps!!.canGetLocation()) {
//                        val latitude = gps!!.latitude
//                        val longitude = gps!!.longitude
//
//                        // \n is for new line
//                        Toast.makeText(
//                            activity,
//                            "Your Location is  this - \nLat: $latitude\nLong: $longitude",
//                            Toast.LENGTH_LONG
//                        ).show()
//                    }
//                }
//            }
//        }
//    }
   override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                    && grantResults[0] === PackageManager.PERMISSION_GRANTED
                ) {
                    Toast.makeText(activity, "You have granted permission", Toast.LENGTH_SHORT)
                        .show()
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(activity, "You need to grant permission", Toast.LENGTH_SHORT)
                        .show()
                }
                return
            }



        }
    }
    private fun setAdapters() {
        adapterPayment =
            SpinnerAdapter_Payment(
                activity,
                paymentlist
            )
        rootView.spin_payment.setAdapter(adapterPayment)

        adapterJoblist =
            SpinnerAdapter_joblist(
                activity,
                joblist
            )
        rootView.spin_type.setAdapter(adapterJoblist)

        adapterState =
            SpinnerAdapter_state(
                activity,
                stateList
            )
        rootView.spin_state.setAdapter(adapterState)
        rootView.spin_locstate.setAdapter(adapterState)

        adapterCity =
            SpinnerAdapter_city(
                activity,
                cityList
            )
        adapterCityLoc =
            SpinnerAdapter_city(
                activity,
                cityList
            )
        rootView.spin_city.setAdapter(adapterCity)
        rootView.spin_loccity.setAdapter(adapterCityLoc)

        try {
            rootView.spin_state.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterState.getItem(position)!!
                    if (user.id != "0") {
                        rootView.spin_city.isEnabled = true
                        stateId = user.id
                        getCity(stateId)

                    }
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }

        try {
            rootView.spin_city.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterCity.getItem(position)!!


                    cityId = user.id

                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }

        try {
            rootView.spin_locstate.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {

                    rootView.spin_loccity.isEnabled=true
                    val user: DropListingDTO = adapterState.getItem(position)!!
                    stateIdLoc = user.id
                    getCity(stateIdLoc)


                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }

        try {
            rootView.spin_loccity.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
//                    val user: DropListingDTO = adapterCityLoc.getItem(position)!!
//                    cityIdLoc = user.id

                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }


        try {
            rootView.spin_payment.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterPayment.getItem(position)!!

                    paymentId = user.id

                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
        try {
            rootView.spin_operation.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    operationid = adapterView!!.getItemAtPosition(position).toString()
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
        try {
            rootView.spin_type.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterJoblist.getItem(position)!!
                    joblistid = user.id

                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }
    }


    fun getListingData() {
        val getMasterRequest =
            VendorRegDataReq()

//        getMasterRequest.serial_code = rootView.edt_capacity.getText().toString()
//        getMasterRequest.setService_name("serviceName")
//
//        vendorActivity!!.apiImp.saveVendorDetails(getMasterRequest)
    }

    private fun setListeners() {
        rootView.add_images!!.setOnClickListener {

            pickImage()
        }

    }

    private fun pickImage() {

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion


        Pix.start(this@VendorRegistration, options)

    }

    fun SaveItem() {
//        ItemReq itemReq = new ItemReq();
//        itemReq.setUsername(expenseActivity.pref.get(Constants.username));
//        itemReq.setPassword(expenseActivity.pref.get(Constants.password));
//        itemReq.setService_name(ApiUtils.ITEM_SAVE);
//        itemReq.setData(items);
//
//        expenseActivity.apiImp.hitApi(itemReq);
    }

    private fun check(): Boolean {
        if (ValidationUtils.isEmpty(this.rootView.edt_name)) {
            vendorActivity?.dialogUtil?.showAlert(getString(R.string.err_name))
            return false
        }
        if (stateId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter State")
            return false
        }
        if (paymentId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter Payment Term")
            return false
        }
        if (operationid.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter Years of Operation")
            return false
        }
        if (cityId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter City")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_pincode)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Pincode")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_address)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Address")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_number)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Number")
            return false
        }
        if (!ValidationUtils.isValidMobileNo(this.rootView.edt_number.text.toString())) {
            vendorActivity?.dialogUtil?.showAlert("Enter correct number")
            return false
        }
        return true
    }

    private fun checkLoc(): Boolean {
        if (ValidationUtils.isEmpty(this.rootView.edt_locname)) {
            vendorActivity?.dialogUtil?.showAlert(getString(R.string.err_name))
            return false
        }
        if (stateId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter State")
            return false
        }

        if (cityId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter City")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_locpincode)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Pincode")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_locaddress)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Address")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_locnumber)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Number")
            return false
        }
        if (!ValidationUtils.isValidMobileNo(this.rootView.edt_locnumber.text.toString())) {
            vendorActivity?.dialogUtil?.showAlert("Enter correct number")
            return false
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        Log.e("TAG", "onActivityResult: ")

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 100) {
                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
//                getImageContentUri(activity!!,returnValue.get(0))

                val fileUri = intent?.data
                Log.e("TAG", "onActivityResult: " + returnValue + " -- " + fileUri)
                val ss = Uri.parse("file://" + returnValue.get(0))

                imageList.add(ImagesDTO(ss.toString()))

                showDynamicImage(ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImagesDTO>
                        }
                    })
            } else {
//                val fileUri = intent?.data
//                val filePath: String = ImagePicker.getFilePath(intent)!!
//                var ii = ImageDTO()
//                ii.image = fileUri
////                Uri.parse(File(picList.toString()).toString())
////                    .toString()//getCompressedUri(fileUri!!, requireActivity()).toString()
//                imageList.add(ii)
//                showDynamicImage(rootView.ll_attach!!,
//                    imageList, object : SelectedMasterCallback {
//                        override fun onSelected(any: Any) {
//                            imageList = any as ArrayList<ImageDTO>
//                        }
//                    })
            }


        }
    }


    fun getTimestamp(): String? {
        val tsLong = System.currentTimeMillis()
        return tsLong.toString()
    }

    fun showDynamicImage(
        llAtach: LinearLayout,
        imageList: ArrayList<ImagesDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)
        Log.e("TAG", "showDynamicImage: " + imageList.size)
        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = ImageItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
                showImageUri(imageDTO.getUri(), context)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Image",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    fun showImageUri(uri: Uri, ctx: Context) {
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.image_view)

        dialog.tiv.setImageURI(uri)

        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)

    }


    companion object {

        @JvmStatic
        var alreadySelectedNames =
            VendorRegReq()


        @JvmStatic
        var partyId: String = ""


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = VendorRegistration()
    }

    override fun onLocationChanged(location: Location) {
        TODO("Not yet implemented")
    }
}