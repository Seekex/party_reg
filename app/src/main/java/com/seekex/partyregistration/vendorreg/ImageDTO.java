package com.seekex.partyregistration.vendorreg;

import android.net.Uri;

public class ImageDTO {
    Uri image;

    public Uri getImage() {
        return image;
    }

    public void setImage(Uri image) {
        this.image = image;
    }
}