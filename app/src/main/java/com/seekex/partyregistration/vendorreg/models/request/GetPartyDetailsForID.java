package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class GetPartyDetailsForID extends RequestBase {

    private GetPartyDetailsForId2  conditions;
    private Integer  page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public GetPartyDetailsForId2 getConditions() {
        return conditions;
    }

    public void setConditions(GetPartyDetailsForId2 conditions) {
        this.conditions = conditions;
    }
}
