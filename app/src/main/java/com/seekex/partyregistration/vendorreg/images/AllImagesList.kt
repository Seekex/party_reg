package com.seekex.partyregistration.vendorreg.images

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.partyregistration.R
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.*
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.*
import com.seekex.scheduleproject.AppConstants
import com.seekx.interfaces.AdapterListener
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ImageUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.vendorreg.AllVendorList
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.images_list.view.*
import kotlinx.android.synthetic.main.jobtype_list.view.*
import kotlinx.android.synthetic.main.location_list.view.*
import kotlinx.android.synthetic.main.vendor_list.view.recyclerView
import java.util.*


/**
 */
class AllImagesList : Fragment() {
    private lateinit var myAdapter: ImagesListAdapter
    private var page = 1
    private var donHit = false
    var mydataList: ArrayList<JobImagesGetResponse2> = java.util.ArrayList()

    public var vendorActivity: VendorActivity? = null
    private lateinit var rootView: View


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.images_list, BasicActi, false)
        vendorActivity = activity as VendorActivity?

        rootView.recyclerView.setLayoutManager(
            LinearLayoutManager(activity)
        )
        rootView.add_image.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AddImagesInfo.instance,
                true
            )
        }
        setAdapter(rootView)
        page = 1
        getImagesList()
        headersettings()



        return rootView
    }

    private fun headersettings() {
        rootView.header.text = "JobType List"
        rootView.back_icon.visibility = View.VISIBLE
        rootView.back_icon.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AllVendorList.instance,
                true
            )

        }
    }

    override fun onResume() {
        super.onResume()
        page = 1
    }

    private fun setAdapter(rootView: View) {
        myAdapter = ImagesListAdapter(object : AdapterListener {
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                jobtypeModel = any1 as JobImagesGetResponse2

                when (i) {
                    2 -> {
                        ImageUtils.showImage(jobtypeModel.image,activity as Context)


                    }
                    1 -> {
                        deleteLocation(jobtypeModel)

                        //hit delete api
                    }

                }

            }

        })
        rootView.recyclerView!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.recyclerView!!.adapter = myAdapter

        rootView.recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !donHit) {
                    Log.e("addOnScrollListener", "false")
                    getImagesList()
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        myAdapter!!.notifyDataSetChanged()
    }

    private fun deleteLocationRequestData(): DeleteDTO {
        val reqData =
            DeleteDTO()


        reqData.setService_name(ApiUtils.IMAGESDELETE)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.id = jobtypeModel.id

        return reqData
    }

    private fun deleteLocation(locationModel: JobImagesGetResponse2) {
        vendorActivity?.apiImp?.deleteLocation(
            deleteLocationRequestData(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {

                        mydataList.remove(locationModel)
                        Toast.makeText(activity, "Record Deleted", Toast.LENGTH_SHORT).show()

                        myAdapter.notifyDataSetChanged()
                    }
                }

            })
    }

    private fun getImagesList() {
        vendorActivity?.apiImp?.getImagesList(
            getallImageReq(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as JobImagesGetResponse
                        val datalist = ss.data

                        if (datalist.size==0) {
                            donHit = true
                        } else {
                            mydataList.addAll(datalist)
                            myAdapter!!.setDataValues(mydataList)
                            myAdapter!!.notifyDataSetChanged()
                            page++
                        }
                    }
                }

            })
    }

    private fun getallImageReq(): GetalljobtypeList {
        val reqData =
            GetalljobtypeList()


        reqData.setService_name(ApiUtils.IMAGESGET)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.page = page;
        reqData.party_id = AllVendorList.dataModel.id



        return reqData
    }

    companion object {

        @JvmStatic
        var jobtypeModel = JobImagesGetResponse2()

        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AllImagesList()
    }
}