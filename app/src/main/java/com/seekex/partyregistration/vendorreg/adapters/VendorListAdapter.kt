package com.seekex.partyregistration.vendorreg.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seekex.partyregistration.databinding.BankAdapterBinding
import com.seekex.partyregistration.vendorreg.models.request.GetPartyDetailsForID
import com.seekex.partyregistration.vendorreg.models.responses.JobWorkedPartyGetResponse2
import com.seekx.interfaces.AdapterListener

import java.util.ArrayList

class VendorListAdapter(private val adapterListener: AdapterListener) :
    RecyclerView.Adapter<VendorListAdapter.ViewHolder>() {

    private var items: ArrayList<JobWorkedPartyGetResponse2> = ArrayList()

    fun setDataValues(items: ArrayList<JobWorkedPartyGetResponse2>?) {
        this.items = items!!

        notifyDataSetChanged()
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = BankAdapterBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }

        binding.editDetail.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 2)
        }
        binding.editImages.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 3)
        }
        binding.editJobtype.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 4)
        }
        binding.editLocation.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 5)
        }
        binding.editDelete.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 6)
        }

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: BankAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: JobWorkedPartyGetResponse2) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}