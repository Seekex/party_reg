package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class VendorRegDataReq extends RequestBase {

    private VendorRegReq data  ;

    public VendorRegReq getData() {
        return data;
    }

    public void setData(VendorRegReq data) {
        this.data = data;
    }
}
