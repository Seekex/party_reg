package com.seekex.partyregistration.vendorreg.vendor_loc

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.seekex.partyregistration.R
import com.seekex.partyregistration.models.apiResponse.DropListingDTO
import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.*
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.*
import com.seekex.scheduleproject.AppConstants
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ValidationUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.vendorreg.AllVendorList
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.location_layout.*
import kotlinx.android.synthetic.main.location_layout.view.*
import java.util.*


/**
 */
class EditLocationInfo : Fragment() {

    public var vendorActivity: VendorActivity? = null
    private lateinit var rootView: View

    var cityList: ArrayList<DropListingDTO> = java.util.ArrayList()
    var mydataList: ArrayList<JobLocationGetResponse2> = java.util.ArrayList()

    var stateId: String = "0"
    var cityId: String = "0"

    private lateinit var adapterState: SpinnerAdapter_state
    private lateinit var adapterCity: SpinnerAdapter_city


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.edt_locationinfo, BasicActi, false)
        vendorActivity = activity as VendorActivity?


        headerSettings()

        setAdapters()

        geJobLocationDetails()
        return rootView
    }

    private fun headerSettings() {

        rootView.header.text = "Edit Location"
        rootView.iv_save.visibility = View.VISIBLE
        rootView.back_icon.visibility = View.VISIBLE

        rootView.back_icon.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AllVendorList.instance,
                true
            )
        }
        rootView.iv_save.setOnClickListener {
            if (checkLoc()){
                updateLocations()

            }

        }
    }


    private fun updateLocations() {
        val reqData =
            SaveLocationdataReq()
        val req =
            SaveLocationdataReq2()

        reqData.setService_name(ApiUtils.SAVELOCATION)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)


        req.id = AllLocationList.locationModel.id
        req.party_id = AllVendorList.dataModel.id
        req.name = edt_locname.text.toString().trim()
        req.address = edt_locaddress.text.toString().trim()
        req.mobile = edt_locnumber.text.toString().trim()
        req.pincode = edt_locpincode.text.toString().trim()
        req.state_id = stateId
        req.city_id = cityId
        req.is_active = true


        reqData.data = req

        vendorActivity?.apiImp?.saveLocation(reqData, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    Log.e("TAG", "onSuccess: ")
                    Toast.makeText(activity, "Record Updated", Toast.LENGTH_SHORT).show()
                    ExtraUtils.changeFragment(
                        vendorActivity!!.supportFragmentManager,
                        AllVendorList.instance,
                        true
                    )
                }
            }
        })
    }
    private fun geyJobLocationDetailREq(): GetPartyDetailsForID {
        val reqData =
            GetPartyDetailsForID()
        val req =
            GetPartyDetailsForId2()

        reqData.setService_name(ApiUtils.GETPARTYLIST)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)
        reqData.page = 1;

        req.id = AllVendorList.dataModel.id
        req.name = ""
        req.mobile =""


        reqData.conditions = req
        return reqData
    }

    private fun geJobLocationDetails() {
        vendorActivity?.apiImp?.getJobLocationDetailById(
            geyJobLocationDetailREq(),
            object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as JobWorkedPartyGetResponse
                        rootView.edt_locname.setText(ss.data.get(0).name)
                        rootView.edt_locaddress.setText(ss.data.get(0).address)
                        rootView.edt_locnumber.setText(ss.data.get(0).mobile)
                        rootView.edt_locpincode.setText(ss.data.get(0).pincode)

                        var ss1 = DropListingDTO(ss.data.get(0).state_id)
                        var ss_city = DropListingDTO(ss.data.get(0).city_id)
                        stateId = ss.data.get(0).state_id
                        cityId = ss.data.get(0).city_id
                        rootView.spin_loccity.isEnabled= true

                        if (AllVendorList.stateListStat.contains(ss1)) {
                            var index = AllVendorList.stateListStat.indexOf(ss1)
                            rootView.spin_locstate.setSelection(index)
                        }
                    }

                }

            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getCity(stateId: String) {
        cityList.clear()
        Log.e("TAG", "state id: " + this.stateId)
        val requestBase =
            getCityReq()
        requestBase.setService_name(ApiUtils.GET_CITY)
        requestBase.token = vendorActivity?.pref?.get(AppConstants.token)
        requestBase.state_id = stateId
        vendorActivity?.apiImp?.getCity(requestBase, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var aa =
                        DropListingDTO(null)
                    aa.id = "0"
                    aa.name = "Select City"

                    cityList.add(aa)
                    var ss = any as DropListingResponse
                    Log.e("TAG", "onSuccess: " + ss.data.size)
                    cityList.addAll(ss.data)
                    adapterCity.notifyDataSetChanged()

                    var ss1 = DropListingDTO(AllVendorList.dataModel.city_id)
                    if (cityList.contains(ss1)) {
                        var index = cityList.indexOf(ss1)
                        rootView.spin_loccity.setSelection(index)


                    }

                }

            }

        })
    }


    private fun setAdapters() {


        adapterState =
            SpinnerAdapter_state(
                activity,
                AllVendorList.stateListStat
            )
        rootView.spin_locstate.setAdapter(adapterState)

        adapterCity =
            SpinnerAdapter_city(
                activity,
                cityList
            )

        rootView.spin_loccity.setAdapter(adapterCity)

        try {
            rootView.spin_locstate.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterState.getItem(position)!!
                    if (user.id != "0") {
                        rootView.spin_loccity.isEnabled = true
                        stateId = user.id
                        getCity(stateId)

                    }
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }

//        try {
//            rootView.spin_loccity.setOnItemSelectedListener(object :
//                AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(
//                    adapterView: AdapterView<*>?, view: View,
//                    position: Int, id: Long
//                ) {
//                    val user: DropListingDTO = adapterCity.getItem(position)!!
//
//                    if (user.id != "0") {
//
//                        cityId = user.id
//                    }
//                }
//
//                override fun onNothingSelected(adapter: AdapterView<*>?) {}
//            })
//        } catch (e: Exception) {
//        }

    }


    private fun checkLoc(): Boolean {
        if (ValidationUtils.isEmpty(this.rootView.edt_locname)) {
            vendorActivity?.dialogUtil?.showAlert(getString(R.string.err_name))
            return false
        }
        if (stateId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter State")
            return false
        }

        if (cityId.equals("0")) {
            vendorActivity?.dialogUtil?.showAlert("Enter City")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_locpincode)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Pincode")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_locaddress)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Address")
            return false
        }
        if (ValidationUtils.isEmpty(this.rootView.edt_locnumber)) {
            vendorActivity?.dialogUtil?.showAlert("Enter Number")
            return false
        }
        if (!ValidationUtils.isValidMobileNo(this.rootView.edt_locnumber.text.toString())) {
            vendorActivity?.dialogUtil?.showAlert("Enter correct number")
            return false
        }
        return true
    }


    companion object {

        @JvmStatic
        var alreadySelectedNames =
            VendorRegReq()

        @JvmStatic
        var partyId: String = ""


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = EditLocationInfo()
    }
}