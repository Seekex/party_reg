
package com.seekex.partyregistration.vendorreg.models.responses;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

public class uploadaudiores extends ResponseBase {

    @SerializedName("data")
    private uploadaudiores2 data;

    public uploadaudiores2 getData() {
        return data;
    }

    public void setData(uploadaudiores2 data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "uploadaudiores{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +
                '}';
    }

}

