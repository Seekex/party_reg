package com.seekex.partyregistration.vendorreg.images

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.github.dhaval2404.imagepicker.ImagePicker
import com.seekex.partyregistration.R
import com.seekex.partyregistration.databinding.ImageItemBinding
import com.seekex.partyregistration.interfaces.SelectedMasterCallback
import com.seekex.partyregistration.models.apiResponse.DropListingDTO
import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.*
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.*
import com.seekex.partyregistration.vendorreg.vendor_loc.ImagesDTO
import com.seekex.scheduleproject.AppConstants
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ValidationUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.vendorreg.AllVendorList
import com.sevenrocks.taskapp.appModules.vendorreg.VendorRegistration
import kotlinx.android.synthetic.main.addimage_layout.*
import kotlinx.android.synthetic.main.addimage_layout.view.*
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.image_view.*
import kotlinx.android.synthetic.main.jobtype_layout.*
import kotlinx.android.synthetic.main.jobtype_layout.view.*
import kotlinx.android.synthetic.main.location_layout.*
import kotlinx.android.synthetic.main.location_layout.view.*
import java.util.*


/**
 */
class AddImagesInfo : Fragment() {

    public var vendorActivity: VendorActivity? = null
    private lateinit var rootView: View
    public var imageList = ArrayList<ImagesDTO>()
    private var call_Counter = 0

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.edt_addimages, BasicActi, false)
        vendorActivity = activity as VendorActivity?
        headerSettings()

        rootView.add_images.setOnClickListener {
            pickImage()
        }

        return rootView
    }

    private fun pickImage() {
//        ImagePicker.with(this)
//            .crop() //Crop image(Optional), Check Customization for more option
//            .compress(1024) //Final image size will be less than 1 MB(Optional)
//            .maxResultSize(
//                1080,
//                1080
//            ) //Final image resolution will be less than 1080 x 1080(Optional)
//            .start()

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion


        Pix.start(this@AddImagesInfo, options)

    }

    private fun uploadImages() {


        vendorActivity?.apiImp?.hitApi(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e("TAG", "onSuccess: filepath=")

                if (status) {
                    var ss = any as uploadaudiores
                    var filepath = ss.data.path + ss.data.filename
                    Log.e("TAG", "onSuccess: filepath=$filepath")
                    vendorActivity?.apiImp?.hitDataApi(
                        AllVendorList.dataModel.id,
                        imageList,
                        filepath,
                        object : ApiCallBack {
                            override fun onSuccess(status: Boolean, any: Any) {
                                if (status) {
                                    Log.e("TAG", "onSuccess: " + imageList.size)
                                    Log.e("TAG", "callcounter: " + call_Counter)
                                    call_Counter++

                                    if (call_Counter < imageList.size) {
                                        uploadImages()
                                    } else {
                                        ExtraUtils.changeFragment(
                                            vendorActivity!!.supportFragmentManager,
                                            AllImagesList.instance,
                                            true
                                        )
                                    }
                                }
                            }

                        })
                }


            }

        })


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        Log.e("TAG", "onActivityResult: ")

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 100) {
                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
//                getImageContentUri(activity!!,returnValue.get(0))

                val fileUri = intent?.data
                Log.e("TAG", "onActivityResult: " + returnValue + " -- " + fileUri)
                val ss= Uri.parse("file://" + returnValue.get(0))

                imageList.add(ImagesDTO(ss.toString()))

                showDynamicImage(ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImagesDTO>
                        }
                    })
            } else {
                val fileUri = intent?.data

                //You can get File object from intent
//            val file: File = ImagePicker.getFile(intent)!!
//            val picList = intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
//            myAdapter.addImage(picList)
                //You can also get File Path from intent
                val filePath: String = ImagePicker.getFilePath(intent)!!
                var ii = ImageDTO()
                ii.image = fileUri

            }
        }
    }

    fun showDynamicImage(
        llAtach: LinearLayout,
        imageList: ArrayList<ImagesDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)
        Log.e("TAG", "showDynamicImage: " + imageList.size)
        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = ImageItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
                showImageUri(imageDTO.getUri(), context)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Image",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    fun showImageUri(uri: Uri, ctx: Context) {
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.image_view)

        dialog.tiv.setImageURI(uri)

        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)

    }

    private fun headerSettings() {

        rootView.header.text = "Add Image"
        rootView.iv_save.visibility = View.VISIBLE
        rootView.back_icon.visibility = View.VISIBLE

        rootView.back_icon.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AllImagesList.instance,
                true
            )
        }
        rootView.iv_save.setOnClickListener {
            uploadImages()
        }
    }

    companion object {

        @JvmStatic
        var alreadySelectedNames =
            VendorRegReq()

        @JvmStatic
        var partyId: String = ""


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AddImagesInfo()
    }
}