package com.seekex.partyregistration.vendorreg.vendor_jobtype

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.seekex.partyregistration.R
import com.seekex.partyregistration.models.apiResponse.DropListingDTO
import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.vendorreg.*
import com.seekex.partyregistration.vendorreg.adapters.*
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.*
import com.seekex.scheduleproject.AppConstants
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ValidationUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.vendorreg.AllVendorList
import com.sevenrocks.taskapp.appModules.vendorreg.VendorRegistration
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.jobtype_layout.*
import kotlinx.android.synthetic.main.jobtype_layout.view.*
import kotlinx.android.synthetic.main.location_layout.*
import kotlinx.android.synthetic.main.location_layout.view.*
import java.util.*


/**
 */
class AddJobtypeInfo : Fragment() {

    public var vendorActivity: VendorActivity? = null
    private lateinit var rootView: View

    var joblistid: String = "0"
    private lateinit var adapterJoblist: SpinnerAdapter_joblist

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.edt_jobtype, BasicActi, false)
        vendorActivity = activity as VendorActivity?


        headerSettings()

        setAdapters()

        return rootView
    }

    private fun headerSettings() {

        rootView.header.text = "Add Jobtype"
        rootView.iv_save.visibility = View.VISIBLE
        rootView.back_icon.visibility = View.VISIBLE

        rootView.back_icon.setOnClickListener {
            ExtraUtils.changeFragment(
                vendorActivity!!.supportFragmentManager,
                AllJobtypeList.instance,
                true
            )
        }
        rootView.iv_save.setOnClickListener {
            if (joblistid.equals("0")) {
                vendorActivity?.dialogUtil?.showAlert("Select JobType")
                return@setOnClickListener
            }
            if (edt_capacity.text.toString().length == 0) {
                vendorActivity?.dialogUtil?.showAlert("Enter Capacity")
                return@setOnClickListener
            }
            updateJobworkValues()
        }
    }
    private fun updateJobworkValues() {
        val reqData =
            SaveJobWorkdataReq()
        val req =
            SaveJobWorkdataReq2()

        reqData.setService_name(ApiUtils.JOBTYPESAVE)
        reqData.token = vendorActivity?.pref?.get(AppConstants.token)

        req.party_id = AllVendorList.dataModel.id
        req.capacity = edt_capacity.text.toString().trim()
        req.job_type_id = joblistid


        reqData.data = req

        vendorActivity?.apiImp?.saveJobtypeData(reqData, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    ExtraUtils.changeFragment(
                        vendorActivity!!.supportFragmentManager,
                        AllJobtypeList.instance,
                        true
                    )
                }

            }

        })


    }


    private fun setAdapters() {
        adapterJoblist =
            SpinnerAdapter_joblist(
                activity,
                AllVendorList.jobtypeListstat
            )
        rootView.spin_type.setAdapter(adapterJoblist)

        try {
            rootView.spin_type.setOnItemSelectedListener(object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?, view: View,
                    position: Int, id: Long
                ) {
                    val user: DropListingDTO = adapterJoblist.getItem(position)!!
                    joblistid = user.id

                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            })
        } catch (e: Exception) {
        }

    }




    companion object {

        @JvmStatic
        var alreadySelectedNames =
            VendorRegReq()

        @JvmStatic
        var partyId: String = ""


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AddJobtypeInfo()
    }
}