package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class SaveLocationdataReq extends RequestBase {

    private SaveLocationdataReq2  data;

    public SaveLocationdataReq2 getData() {
        return data;
    }

    public void setData(SaveLocationdataReq2 data) {
        this.data = data;
    }
}
