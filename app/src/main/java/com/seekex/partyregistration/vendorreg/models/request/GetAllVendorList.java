package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class GetAllVendorList extends RequestBase {

    private GetAllVendorList2  conditions;
    private Integer  page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public GetAllVendorList2 getConditions() {
        return conditions;
    }

    public void setConditions(GetAllVendorList2 conditions) {
        this.conditions = conditions;
    }
}
