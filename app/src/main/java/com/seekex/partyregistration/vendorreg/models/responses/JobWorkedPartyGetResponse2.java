
package com.seekex.partyregistration.vendorreg.models.responses;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

import java.util.Objects;

public class JobWorkedPartyGetResponse2 extends ResponseBase {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("payment_term_id")
    private String payment_term_id;
    @SerializedName("bank_name")
    private String bank_name;
    @SerializedName("bank_ifsc_code")
    private String bank_ifsc_code;
    @SerializedName("bank_account")
    private String bank_account;
    @SerializedName("state_id")
    private String state_id;
    @SerializedName("city_id")
    private String city_id;
    @SerializedName("pincode")
    private String pincode;
    @SerializedName("address")
    private String address;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("year_of_experince")
    private String year_of_experince;
    @SerializedName("is_customer")
    private Boolean is_customer;
    @SerializedName("is_supplier")
    private Boolean is_supplier;
    @SerializedName("is_job_worker")
    private Boolean is_job_worker;
    @SerializedName("is_ecommerce")
    private Boolean is_ecommerce;
    @SerializedName("is_manual_mapping")
    private Boolean is_manual_mapping;
    @SerializedName("gst_no")
    private String gst_no;
    @SerializedName("pan_no")
    private String pan_no;

    @SerializedName("tin_no")
    private String tin_no;
    @SerializedName("state")
    private String state;
    @SerializedName("city")
    private String city;
    public String getStatetext() {
        return "State: "+state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    public String getCitytext() {
        return "City: "+city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getNametext() {
        return "Name: "+name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayment_term_id() {
        return payment_term_id;
    }

    public void setPayment_term_id(String payment_term_id) {
        this.payment_term_id = payment_term_id;
    }
    public String getBank_nametext() {
        return "Bank Name: "+bank_name;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_ifsc_code() {
        return bank_ifsc_code;
    }

    public void setBank_ifsc_code(String bank_ifsc_code) {
        this.bank_ifsc_code = bank_ifsc_code;
    }

    public String getBank_account() {
        return bank_account;
    }

    public void setBank_account(String bank_account) {
        this.bank_account = bank_account;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }
    public String getPincodetext() {
        return "Pincode: "+pincode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }
    public String getAddresstext() {
        return "Address: "+address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getMobiletext() {
        return "Mobile: "+mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getYear_of_experincetext() {
        return "Experience in years"+year_of_experince;
    }

    public String getYear_of_experince() {
        return year_of_experince;
    }

    public void setYear_of_experince(String year_of_experince) {
        this.year_of_experince = year_of_experince;
    }

    public Boolean getIs_customer() {
        return is_customer;
    }

    public void setIs_customer(Boolean is_customer) {
        this.is_customer = is_customer;
    }

    public Boolean getIs_supplier() {
        return is_supplier;
    }

    public void setIs_supplier(Boolean is_supplier) {
        this.is_supplier = is_supplier;
    }

    public Boolean getIs_job_worker() {
        return is_job_worker;
    }

    public void setIs_job_worker(Boolean is_job_worker) {
        this.is_job_worker = is_job_worker;
    }

    public Boolean getIs_ecommerce() {
        return is_ecommerce;
    }

    public void setIs_ecommerce(Boolean is_ecommerce) {
        this.is_ecommerce = is_ecommerce;
    }

    public Boolean getIs_manual_mapping() {
        return is_manual_mapping;
    }

    public void setIs_manual_mapping(Boolean is_manual_mapping) {
        this.is_manual_mapping = is_manual_mapping;
    }
    public String getGst_notext() {
        return "Gst no: "+gst_no;
    }

    public String getGst_no() {
        return gst_no;
    }

    public void setGst_no(String gst_no) {
        this.gst_no = gst_no;
    }

    public String getPan_no() {
        return pan_no;
    }

    public void setPan_no(String pan_no) {
        this.pan_no = pan_no;
    }

    public String getTin_no() {
        return tin_no;
    }

    public void setTin_no(String tin_no) {
        this.tin_no = tin_no;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobWorkedPartyGetResponse2 that = (JobWorkedPartyGetResponse2) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

