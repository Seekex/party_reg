
package com.seekex.partyregistration.vendorreg.models.responses;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

import java.util.Objects;

public class JobImagesGetResponse2 extends ResponseBase {



    @SerializedName("id")
    private String id;
    @SerializedName("party_id")
    private String party_id;
    @SerializedName("image")
    private String image;
    @SerializedName("is_upload_to_s3")
    private String is_upload_to_s3;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIs_upload_to_s3() {
        return is_upload_to_s3;
    }

    public void setIs_upload_to_s3(String is_upload_to_s3) {
        this.is_upload_to_s3 = is_upload_to_s3;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

