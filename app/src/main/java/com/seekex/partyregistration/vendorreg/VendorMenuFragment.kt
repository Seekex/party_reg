package com.seekex.partyregistration.vendorreg

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.seekex.partyregistration.ChooseAdapter
import com.seekex.partyregistration.GPSTracker
import com.seekex.partyregistration.R
import com.seekex.partyregistration.activities.LoginActivity
import com.seekex.scheduleproject.AppConstants
import com.seekx.utils.DataUtils
import com.seekx.utils.ExtraUtils
import com.sevenrocks.taskapp.appModules.vendorreg.AllVendorList
import com.sevenrocks.taskapp.appModules.vendorreg.VendorRegistration
import kotlinx.android.synthetic.main.choose_activty.view.*
import kotlinx.android.synthetic.main.header.view.*
import java.util.*

class VendorMenuFragment : Fragment() {
    private var vendorActivity: VendorActivity? = null
    var gps: GPSTracker? = null

    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.choose_activty, BasicActi, false)
        init()
        setListener()
        getGPSLocation()
        rootView.header.setText("Manage Vendors")
        rootView.logout.visibility = View.VISIBLE
        rootView.logout.setOnClickListener{
            vendorActivity?.logoutt()
        }

        return rootView
    }


    private fun init() {
        vendorActivity = activity as VendorActivity?
        setAapter()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                    && grantResults[0] === PackageManager.PERMISSION_GRANTED
                ) {
                    Toast.makeText(activity, "You have granted permission", Toast.LENGTH_SHORT)
                        .show()
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(activity, "You need to grant permission", Toast.LENGTH_SHORT)
                        .show()
                }
                return
            }



        }
    }

    private fun getGPSLocation() {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        } else {
//            Toast.makeText(activity, "You need have granted permission", Toast.LENGTH_SHORT).show()
            gps = GPSTracker(activity, activity)

            // Check if GPS enabled
            if (gps!!.canGetLocation()) {
                val latitude = gps!!.latitude
                val longitude = gps!!.longitude

                VendorMenuFragment.latitude=latitude
                VendorMenuFragment.longitude=longitude
                // \n is for new line
                if (latitude==0.0){
                    Toast.makeText(
                        activity,
                        "No Location Fetched", Toast.LENGTH_LONG
                    ).show()
                }else{
                }

            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps!!.showSettingsAlert()
            }
        }

    }
    private fun getlocation(latitude: Double, longitude: Double) {
        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(activity, Locale.getDefault())

        addresses = geocoder.getFromLocation(
            latitude,
            longitude,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5


        val address =
            addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        val city = addresses[0].locality
        val state = addresses[0].adminArea
        val country = addresses[0].countryName
        val postalCode = addresses[0].postalCode
        val knownName = addresses[0].featureName // Only if available else return NULL

        Log.e("TAG", "getlocation: "+city )

    }
    private fun setListener() {}
    private fun setAapter() {
        val chooseAdapter: ChooseAdapter = object : ChooseAdapter(
            context, DataUtils.getVendorData(requireActivity())

        ) {
            protected override fun onViewClicked(v: View?, s: String?) {
                when (s) {
                    "1" -> ExtraUtils.changeFragment(
                        vendorActivity!!.supportFragmentManager,
                        VendorRegistration.instance,
                        true
                    )
                    "2" -> ExtraUtils.changeFragment(
                        vendorActivity!!.supportFragmentManager,
                        AllVendorList.instance,
                        true
                    )

                }
            }
        }
        rootView.gv.setAdapter(chooseAdapter)
        rootView.gv.setExpanded(true)
    }


    companion object {

        @JvmField
        var latitude : Double= 0.0
        var longitude : Double=0.0

        @JvmStatic
        val instance: Fragment
            get() = VendorMenuFragment()
    }
}