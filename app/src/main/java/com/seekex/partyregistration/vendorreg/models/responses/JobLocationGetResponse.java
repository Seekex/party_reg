
package com.seekex.partyregistration.vendorreg.models.responses;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

import java.util.ArrayList;

public class JobLocationGetResponse extends ResponseBase {

    @SerializedName("total_pages")
    private Integer total_pages;

    @SerializedName("current_page")
    private Integer current_page;
    @SerializedName("limit")
    private Integer limit;

    @SerializedName("data")
    private ArrayList<JobLocationGetResponse2> data;


    public Integer getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(Integer total_pages) {
        this.total_pages = total_pages;
    }

    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public ArrayList<JobLocationGetResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<JobLocationGetResponse2> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

