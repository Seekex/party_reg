package com.seekex.partyregistration.vendorreg.vendor_loc

import android.net.Uri

class ImagesDTO(
    var image: String){

    fun getUri(): Uri {
        return  Uri.parse(image)
    }
}