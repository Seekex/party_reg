
package com.seekex.partyregistration.vendorreg.models.responses;

import com.google.gson.annotations.SerializedName;
import com.seekex.partyregistration.retrofitclasses.ResponseBase;

import java.util.Objects;

public class JobTypeGetResponse2 extends ResponseBase {



    @SerializedName("id")
    private String id;
    @SerializedName("party_id")
    private String party_id;
    @SerializedName("job_type_id")
    private String job_type_id;
    @SerializedName("capacity")
    private String capacity;
    @SerializedName("job_type")
    private String job_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getJob_type_id() {
        return job_type_id;
    }

    public void setJob_type_id(String job_type_id) {
        this.job_type_id = job_type_id;
    }
    public String getCapacityText() {
        return "Capacity: "+capacity;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getJob_type() {
        return job_type;
    }
    public String getJob_typeText() {
        return "Job Type: "+job_type;
    }
    public void setJob_type(String job_type) {
        this.job_type = job_type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobTypeGetResponse2 that = (JobTypeGetResponse2) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Login{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +

                '}';
    }

}

