package com.seekex.partyregistration.vendorreg.models.request;


import com.seekex.partyregistration.retrofitclasses.RequestBase;

public class SaveJobWorkdataReq2  {

    private String  party_id;
    private String  job_type_id;
    private String  capacity;
    private String  id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getJob_type_id() {
        return job_type_id;
    }

    public void setJob_type_id(String job_type_id) {
        this.job_type_id = job_type_id;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }
}
