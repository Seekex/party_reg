package com.seekex.partyregistration.retrofitclasses.interfaces;

public interface ApiCallBacknew {
    void onSuccess(String serviceName,Object object);
    void onFailed(String msg);
}
