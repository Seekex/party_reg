package com.seekx.webService

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.seekex.partyregistration.R
import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.models.apiResponse.LoginResponse
import com.seekex.partyregistration.retrofitclasses.RequestBase
import com.seekex.partyregistration.retrofitclasses.ResponseBase
import com.seekex.partyregistration.retrofitclasses.interfaces.ApiCallBacknew
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.*
import com.seekex.partyregistration.vendorreg.vendor_loc.ImagesDTO
import com.seekex.scheduleproject.AppConstants
import com.seekex.scheduleproject.Preferences
import com.seekex.scheduleproject.custombinding.CustomLoader
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ImageUtils
import com.seekx.webService.interfaces.ApiCallBack
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File
import java.util.ArrayList


open class ApiImp(val context: Context) {

    var pref: Preferences = Preferences(context)
    var dialogUtil: DialogUtils = DialogUtils(context)


    companion object {
        const val TAG = "ApiImpResp"
    }


    private val webApi = ApiUtils.getWebApi(context)
    private var apiCallBacknew: ApiCallBacknew? = null

    var customLoader: CustomLoader = CustomLoader(context)
    private val uploadFileApi = ApiUtils.getUploadApi(context)

    fun showProgressBar() {
        customLoader.show()
    }

    fun cancelProgressBar() {
        customLoader.cancel()
    }

    fun setApiCallBacknew(apiCallBacknew: ApiCallBacknew?) {
        this.apiCallBacknew = apiCallBacknew
    }


    fun login(number :RequestBase, apiCallBack: ApiCallBack) {
        Log.e(TAG, "checkCanNumber: "+Gson().toJson(number) )
        if (!onStartApi())
            return
        ApiUtils.getWebApi(context).login(number).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LoginResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: LoginResponse) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getStates(requestBase: RequestBase, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "checkCanNumber: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getPaymentTerms(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<DropListingResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: DropListingResponse) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getCity(requestBase: getCityReq, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "checkCanNumber: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getCity(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<DropListingResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: DropListingResponse) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getPaymentTerms(requestBase: RequestBase, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "checkCanNumber: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getPaymentTerms(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<DropListingResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: DropListingResponse) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveBasicForm(requestBase: VendorRegDataReq, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "saveBasicForm: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).saveBData(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<VendorSaveResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: VendorSaveResponse) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun deleteParty(requestBase: DeleteDTO, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "getPartyDetailForId: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).deleteLocation(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ResponseBase> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: ResponseBase) {
                    Log.e("getPartyDetailForId res", ": " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun deleteLocation(requestBase: DeleteDTO, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "getPartyDetailForId: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).deleteLocation(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ResponseBase> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: ResponseBase) {
                    Log.e("getPartyDetailForId res", ": " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getImagesList(requestBase: GetalljobtypeList, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "getPartyDetailForId: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getImagesList(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<JobImagesGetResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: JobImagesGetResponse) {
                    Log.e("getPartyDetailForId res", ": " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getJobtypeList(requestBase: GetalljobtypeList, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "getPartyDetailForId: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getJobtypeList(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<JobtypeGetResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: JobtypeGetResponse) {
                    Log.e("getPartyDetailForId res", ": " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getLocationList(requestBase: GetallLocationList, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "getPartyDetailForId: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getLocationList(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<JobLocationGetResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: JobLocationGetResponse) {
                    Log.e("getPartyDetailForId res", ": " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getJobtypedetailById(requestBase: GetalljobtypeList, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "getPartyDetailForId: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getJobTypeById(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<JobtypeGetResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: JobtypeGetResponse) {
                    Log.e("getPartyDetailForId res", ": " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }



    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getJobLocationDetailById(requestBase: GetPartyDetailsForID, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "getPartyDetailForId: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getJobDataByID(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<JobWorkedPartyGetResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: JobWorkedPartyGetResponse) {
                    Log.e("getPartyDetailForId res", ": " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getAllJobList(requestBase: GetAllVendorList, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "getPartyDetailForId: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getJobData(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<JobWorkedPartyGetResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: JobWorkedPartyGetResponse) {
                    Log.e("getPartyDetailForId res", ": " + respnse.data.get(0).state_id)
                    Log.e("getPartyDetailForId res", ": " + respnse.data.get(0).city)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveLocation(requestBase: SaveLocationdataReq, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "saveBasicForm: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).saveLocation(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ResponseBase> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: ResponseBase) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveJobtypeData(requestBase: SaveJobWorkdataReq, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return

        Log.e(TAG, "saveBasicForm: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).saveJoborderData(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<VendorSaveResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: VendorSaveResponse) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getListing(requestBase: RequestBase, apiCallBack: ApiCallBack) {
        if (!onStartApinew(requestBase))
            return
        Log.e(TAG, "checkCanNumber: "+Gson().toJson(requestBase) )

        ApiUtils.getWebApi(context).getJobType(requestBase).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<DropListingResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("checkCanNumber error ", "file: " + e.printStackTrace())
                    Log.e("checkCanNumber error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: DropListingResponse) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }
    fun getRealPathFromURIPath(contentURI: Uri, activity: Context): String? {
        var filePath= contentURI.path

        return try {
//                val proj = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
            if (cursor == null) {
                filePath
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }
        }catch (e: Exception){
            filePath= filePath?.replace("/external_files", "")
            Environment.getExternalStorageDirectory().absolutePath+filePath
        }

    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun hitApi(counter: Int, imagelist: ArrayList<ImagesDTO>, apiCallBack: ApiCallBack) {
        if (!onStartApi())
            return
//        Log.e(TAG, "checkCanNumber: "+Gson().toJson(requestBase) )

        val filePath =getRealPathFromURIPath(imagelist.get(counter).getUri(), context)

        val file = File(filePath!!)

        val mFile: RequestBody
        mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        Log.e("hitApi 11 ", "uploadFile: " + file)
        Log.e("hitApi 11 ", "uploadFile: name " +  file.name)
//        val reportReq = ReportReq(list.get(i).call_datetime, list.get(i).phoneNumber, list.get(i).rec_file_path)

        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi(context).uploadSingleFile(fileToUpload).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<uploadaudiores> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    Log.e("upload error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: uploadaudiores) {
                    Log.e("upload res", "file: " + respnse)
                    Log.e("upload res", "file: " + respnse.data.file)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }
    fun hitDataApi(partyid: String, list: ArrayList<ImagesDTO>, filepath: String, apiCallBack: ApiCallBack) {

        if (!onStartApi())
            return
        val reqData =
            VendorUploadmageReq()

        reqData.setService_name(ApiUtils.SAVEIMAGEFILE)
        reqData.token = pref?.get(AppConstants.token)
        reqData.file = filepath
        reqData.party_id = partyid


        Log.e("hitDataApi 11 ", "reportReq:i== " + Gson().toJson(reqData))

        ApiUtils.getWebApi(context).saveaudiodata(reqData)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<ResponseBase> {
                override fun onCompleted() {}
                override fun onError(e: Throwable) {
                    Log.e("inbox api", "err: " + e.toString())

                    onResponseApi(e.toString(), apiCallBack)
                }

                override fun onNext(respnse: ResponseBase) {
                    Log.e("inbox api", "onNext: " + respnse.toString())
                    onResponseApi(respnse, apiCallBack)
                }
            })
    }

//    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
//    fun gettasks(activityReq: RequestBase) {
//        if (!onStartApi(activityReq)) return
//
//        Log.e("TAG", "activityRequest: " + Gson().toJson(activityReq))
//        webApi.getTasks(activityReq)?.subscribeOn(Schedulers.io())
//            ?.observeOn(AndroidSchedulers.mainThread())
//            ?.subscribe(object : Observer<TaskResponse> {
//                override fun onCompleted() {}
//
//                override fun onError(e: Throwable) {
//                    Log.e(TAG, "onError activity: " + e.toString())
//                    apiCallBacknew!!.onFailed(e.toString())
////                    onResponseApi(e.toString(), null)
//                }
//
//                @SuppressLint("LongLogTag")
//                override fun onNext(respnse: TaskResponse) {
//                    Log.e(TAG, "get activity: " + respnse.toString())
//                    Log.v(ApiUtils.LOGIN, respnse.toString())
//                    if (respnse.getStatus() == 0) apiCallBacknew!!.onFailed(respnse.getMsg()) else {
//                        apiCallBacknew!!.onSuccess(respnse.getService_name(), respnse)
//                    }
//                    onResponseApi(respnse.getStatus() == 0, respnse.getMsg())
//
//
//                }
//
//            })
//    }

    private fun onStartApiWithoutLoader(): Boolean {
        return checkInternet()
    }

    private fun checkInternet(): Boolean {
        if (!ExtraUtils.isNetworkConnectedMainThread(context)) {
            dialogUtil.showAlert(context.getString(R.string.internet_error))
            return false
        }
        return true
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onStartApi(`object`: RequestBase): Boolean {
        if (!isNetworkConnectedMainThred(context)) {
            dialogUtil.showAlert("No Internet Connection")
            return false
        }
        `object`.setUsername(pref.get(AppConstants.user_name))
        `object`.setPassword(pref.get(AppConstants.password))
        `object`.app_version = context!!.getString(R.string.version)
        `object`.secret_key = ImageUtils.getSecretKey()
        `object`.token = pref.get(AppConstants.token)
        if (!(context as Activity?)!!.isDestroyed) customLoader.show()
        return true
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onStartApinew(`object`: RequestBase): Boolean {
        if (!isNetworkConnectedMainThred(context)) {
            dialogUtil.showAlert("No Internet Connection")
            return false
        }
//        `object`.setUsername(pref.get(AppConstants.user_name))
//        `object`.setPassword(pref.get(AppConstants.password))
//        `object`.app_version = context!!.getString(R.string.version)
//        `object`.token = pref.get(AppConstants.token)
        if (!(context as Activity?)!!.isDestroyed) customLoader.show()
        return true
    }
    private fun onStartApi(): Boolean {
        if (!checkInternet()) {
            dialogUtil.showAlert(context.getString(R.string.internet_error))
            return false
        }
        if (!(context as Activity?)!!.isDestroyed) customLoader.show()
        return true

    }

    private fun checkResMsg(msg: String): String {
        if (msg.contains("java.net") || msg.contains("retrofit2.adapter.rxjava")) {
//            toast(Preferences(context).get(AppConstants.token))
            return context.getString(R.string.connection_failed)
        }

        return msg
    }

    private fun onResponseApi(response: ResponseBase, Obj: Any?, apiCallBack: ApiCallBack) {
        customLoader.cancel()
        if (response.status == 0) {

            dialogUtil.showAlert(checkResMsg(response.msg))
            apiCallBack.onSuccess(false, response.msg)
        } else {
            apiCallBack.onSuccess(true, Obj!!)
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onResponseApi(status: Boolean, msg: String?) {
        if (status) {
            dialogUtil.showAlert(msg!!)
        }
        if (!(context as Activity?)!!.isDestroyed) customLoader.cancel()
    }

    private fun onResponseApi(response: ResponseBase, apiCallBack: ApiCallBack?) {
        customLoader.cancel()
        if (response.status == 0) {

//            if (onLogoutCheck(response))
//                return

            dialogUtil.showAlert(checkResMsg(response.msg))
        }
        if (response.status == 0) {
            apiCallBack?.onSuccess(false, response)

        } else {
            apiCallBack?.onSuccess(true, response)

        }

    }

    private fun onResponseApi(msg: String, apiCallBack: ApiCallBack?) {
        customLoader.cancel()
        Log.e("upload 44", "onResponseApi:msg= " + msg)

        dialogUtil.showAlert(checkResMsg(msg))

        apiCallBack?.onSuccess(false, msg)

    }

    @SuppressLint("MissingPermission")
    open fun isNetworkConnectedMainThred(ctx: Context): Boolean {
        val cm = ctx
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ni = cm.activeNetworkInfo
        return if (ni == null) {
//            DialogUtil.showInternetError(ctx)
            false
        } else true
    }


}