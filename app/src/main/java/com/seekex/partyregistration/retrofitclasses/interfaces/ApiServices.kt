package com.seekx.webService.interfaces

import com.seekex.partyregistration.models.apiResponse.DropListingResponse
import com.seekex.partyregistration.models.apiResponse.LoginResponse
import com.seekex.partyregistration.retrofitclasses.RequestBase
import com.seekex.partyregistration.retrofitclasses.ResponseBase
import com.seekex.partyregistration.vendorreg.models.request.*
import com.seekex.partyregistration.vendorreg.models.responses.*
import com.seekx.webService.ApiUtils
import okhttp3.MultipartBody
import retrofit2.http.*
import rx.Observable


interface ApiServices {


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun login(@Body loginReq: RequestBase): Observable<LoginResponse>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getJobType(@Body loginReq: RequestBase): Observable<DropListingResponse>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getPaymentTerms(@Body loginReq: RequestBase): Observable<DropListingResponse>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getCity(@Body loginReq: getCityReq): Observable<DropListingResponse>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveBData(@Body loginReq: VendorRegDataReq): Observable<VendorSaveResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveJoborderData(@Body loginReq: SaveJobWorkdataReq): Observable<VendorSaveResponse>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getJobData(@Body loginReq: GetAllVendorList): Observable<JobWorkedPartyGetResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getJobDataByID(@Body loginReq: GetPartyDetailsForID): Observable<JobWorkedPartyGetResponse>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getJobTypeById(@Body loginReq: GetalljobtypeList): Observable<JobtypeGetResponse>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getLocationList(@Body loginReq: GetallLocationList): Observable<JobLocationGetResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getJobtypeList(@Body loginReq: GetalljobtypeList): Observable<JobtypeGetResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getImagesList(@Body loginReq: GetalljobtypeList): Observable<JobImagesGetResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun deleteLocation(@Body loginReq: DeleteDTO): Observable<ResponseBase>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun deleteParty(@Body loginReq: DeleteDTO): Observable<ResponseBase>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveLocation(@Body loginReq: SaveLocationdataReq): Observable<ResponseBase>


    @Multipart
    @POST(ApiUtils.UPLOAD_REC_FILE)
    fun uploadSingleFile(@Part file: MultipartBody.Part?): Observable<uploadaudiores>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveaudiodata(@Body `object`: VendorUploadmageReq?): Observable<ResponseBase>?
}