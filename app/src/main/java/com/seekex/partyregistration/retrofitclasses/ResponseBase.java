package com.seekex.partyregistration.retrofitclasses;

import org.jetbrains.annotations.NotNull;

public class ResponseBase {

    public String msg;
    public int status;
    public String service_name;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    @NotNull
    @Override
    public String toString() {
        return "ResponseBase{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +
                '}';
    }
}
