package com.seekx.webService

import android.content.Context
import android.util.Log
import com.seekex.scheduleproject.AppConstants
import com.seekex.scheduleproject.Preferences
import com.seekx.webService.interfaces.ApiServices

class ApiUtils {

    companion object {
        const val APIPREFIX = "https://stagging.sevenrocks.in/v2-web-api/"
        const val UPLOAD_REC_FILE = "https://stagging.sevenrocks.in/CronJobs/ajaxUploadTempFile"

        const val API_HEADER = "Content-Type: application/json;charset=UTF-8"

        const val LOGIN =  "login"
        const val GET_PAYMENT_TERMS =  "get_payment_terms_for_job_worker"
        const val GET_JOB_TYPES =  "get_job_types"
        const val GET_STATES =  "get_states"
        const val GET_CITY =  "get_cities"
        const val SAVE_PARTY =  "job_worker_party_save"
        const val GETPARTYLIST =  "job_worker_party_get"
        const val DELETEPARTY =  "job_worker_party_delete"
        const val SAVEIMAGEFILE =  "job_worker_party_photo_save"
        const val IMAGESGET =  "job_worker_party_photo_get"
        const val IMAGESDELETE =  "job_worker_party_photo_delete"
        const val SAVELOCATION =  "job_worker_party_location_save"
        const val GETLOCATION =  "job_worker_party_location_get"
        const val DELETELOCATION =  "job_worker_party_location_delete"
        const val JOBTYPESAVE =  "job_worker_party_job_type_save"
        const val JOBTYPEGET =  "job_worker_party_job_type_get"
        const val JOBTYPEDELETE =  "job_worker_party_job_type_delete"


//        const val localUrl = "http://54.172.167.148:3333/"
        const val localUrl = "https://sevenrocks.in/"


        fun getWebApi(context: Context): ApiServices {
            var pref: Preferences
            pref = Preferences(context!!)
            Log.e("TAG", "getWebApi: " + pref.get(AppConstants.BASEURL)+"v2-web-api/")
            return RetrofitClient.getClient(pref.get(AppConstants.BASEURL)+"v2-web-api/")!!.create(ApiServices::class.java)
        }
        fun getUploadApi(context: Context): ApiServices {
            var pref: Preferences
            pref = Preferences(context!!)
            Log.e("TAG", "getWebApi: " + pref.get(AppConstants.BASEURL)+"v2-web-api/")
            return RetrofitClient.getUploadFileClient(pref.get(AppConstants.BASEURL)+"v2-web-api/")!!.create(ApiServices::class.java)
        }

    }

}