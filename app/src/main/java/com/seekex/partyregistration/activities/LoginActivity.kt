package com.seekex.partyregistration.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.seekex.partyregistration.R
import com.seekex.partyregistration.models.apiResponse.LoginResponse
import com.seekex.partyregistration.retrofitclasses.RequestBase
import com.seekex.partyregistration.retrofitclasses.interfaces.ApiCallBacknew
import com.seekex.partyregistration.vendorreg.VendorActivity
import com.seekex.scheduleproject.AppConstants
import com.seekex.scheduleproject.Preferences
import com.seekex.scheduleproject.utils.ActivityUtils
import com.seekx.utils.*
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.apiRequest.LoginReq
import kotlinx.android.synthetic.main.loginactivity.*

import java.util.*

class LoginActivity : AppCompatActivity() {
    lateinit var pref: Preferences
    lateinit var dialogUtils: DialogUtils
    lateinit var editor: SharedPreferences.Editor
    lateinit var apiImp: ApiImp
    lateinit var sharedpreferences: SharedPreferences
    val MyPREFERENCES = "MyPrefs"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loginactivity)

        init()
//        setPrefernce()
//        setListener()
        login_button.setOnClickListener {
            loginfunc()
        }
    }

    private fun loginfunc() {
        val loginReq = RequestBase()
        loginReq.setService_name(ApiUtils.LOGIN)
        loginReq.username = et_username.text.toString().trim()
        loginReq.password = et_pass.text.toString().trim()
        apiImp?.login(loginReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e("TAG", "onSuccess: $status")
                if (status) {
                    var ss = any as LoginResponse
                    Log.e("TAG", "onSuccess: ${ss.token}")

                    pref.set(AppConstants.token, ss.token)
                   pref.setBoolean(AppConstants.isLogin, true)
                    intent = Intent(applicationContext, VendorActivity::class.java)
                    startActivity(intent)
                    finish()
                }


            }
        })
    }

    private fun setPrefernce() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
//        editor = sharedpreferences.edit()
    }

    private fun init() {
        dialogUtils = DialogUtils(this)
        apiImp = ApiImp(this)
        pref = Preferences(this)

    }

//
//    private fun setListener() {
//
////        login_button.setOnClickListener {
////            apiImp?.showProgressBar()
////            if (check())
////                validateUser(
////                    ExtraUtils.getVal(et_username),
////                    ExtraUtils.getVal(et_pass)
////                )
////            else
////                apiImp?.cancelProgressBar()
////        }
//
//
//    }
//
//    fun validateUser(username: String, pass: String) {
//        val loginReq = LoginReq()
////        loginReq.gcm_reg_no = "abc"
////        loginReq.otp = ""
//        loginReq.username = username
//        loginReq.password = pass
//        loginReq.setService_name(ApiUtils.LOGIN)
////        loginReq.secret_key = ImageUtils.getSecretKey()
//
//        Log.e("TAG", "validateUser: " + username)
//        apiImp.login(loginReq)
//
//    }
//
//    override fun onResume() {
//        super.onResume()
//    }
//
//    private fun check(): Boolean {
//        return when {
//            ValidationUtils.isEmpty(et_username) -> {
//                dialogUtils.showAlert(getString(R.string.error_username))
//                false
//            }
//            ValidationUtils.isContainsSpace(et_username) -> {
//                dialogUtils.showAlert(getString(R.string.error_username_space))
//                false
//            }
//            ValidationUtils.isEmpty(et_pass) -> {
//                dialogUtils.showAlert(getString(R.string.err_password))
//                false
//            }
//            else -> true
//        }
//
//    }
//
//    override fun onSuccess(serviceName: String?, `object`: Any?) {
//        val detail = `object` as LoginResponse
//        pref.set(AppConstants.uid, detail.user.id!!.toString()).commit()
//        pref.set(AppConstants.pic_url, detail.user.profile_image_thumbnail).commit()
//        pref.set(AppConstants.name, detail.user.name).commit()
//        pref.setBoolean(AppConstants.isLogin, true).commit()
//        pref.set(AppConstants.user_name, et_username.text.toString()).commit()
//        pref.set(AppConstants.password, et_pass.text.toString()).commit()
//        pref.set(AppConstants.token, detail.token).commit()
//        ActivityUtils.navigate(this@LoginActivity, MainActivity::class.java, false)
//
//    }
//
//    override fun onFailed(msg: String?) {
//        TODO("Not yet implemented")
//    }
}