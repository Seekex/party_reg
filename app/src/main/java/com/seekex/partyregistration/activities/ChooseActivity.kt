package com.seekex.partyregistration.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.seekex.partyregistration.R
import com.seekex.scheduleproject.AppConstants
import com.seekex.scheduleproject.Preferences

import com.seekx.utils.*
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.models.apiRequest.LoginReq
import kotlinx.android.synthetic.main.chooseactivity.*
import kotlinx.android.synthetic.main.loginactivity.*
import kotlinx.android.synthetic.main.loginactivity.view.*
import java.util.*

class ChooseActivity : AppCompatActivity() {
    lateinit var pref: Preferences
    lateinit var dialogUtils: DialogUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chooseactivity)

        init()
        setListener()

    }

    private fun init() {
        dialogUtils = DialogUtils(this)
        pref = Preferences(this)

    }


    private fun setListener() {
        chk_live.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                chk_stagging.isChecked=false
                chk_test.isChecked=false
                pref.set(AppConstants.BASEURL, AppConstants.LIVEURL)
            }

        }
        chk_test.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                chk_stagging.isChecked=false
                chk_live.isChecked=false
                Toast.makeText(this, isChecked.toString(), Toast.LENGTH_SHORT).show()
                pref.set(AppConstants.BASEURL,AppConstants.TESTURL)
            }


        }
        chk_stagging.setOnCheckedChangeListener { buttonView, isChecked ->
            chk_live.isChecked=false
            chk_test.isChecked=false
            pref.set(AppConstants.BASEURL,AppConstants.STAGGINGURL)
        }
        btn_next.setOnClickListener{
            val i = Intent(this@ChooseActivity, LoginActivity::class.java)
            startActivity(i)
            finish()
        }

    }


}