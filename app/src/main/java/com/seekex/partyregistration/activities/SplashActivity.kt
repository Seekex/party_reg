package com.seekex.partyregistration.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.seekex.partyregistration.R
import com.seekex.partyregistration.vendorreg.VendorActivity
import com.seekex.scheduleproject.AppConstants
import com.seekex.scheduleproject.Preferences
import com.seekx.webService.ApiImp

class SplashActivity : AppCompatActivity() {

    var editor: SharedPreferences.Editor? = null
    lateinit var apiImp: ApiImp
    var sharedpreferences: SharedPreferences? = null
    val MyPREFERENCES = "MyPrefs"
    lateinit var pref: Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        pref = Preferences(this)

//        setPrefernce()

        Handler().postDelayed({ // This method will be executed once the timer is over

            if (pref.getBoolean(AppConstants.isLogin)){
                val i = Intent(this@SplashActivity, VendorActivity::class.java)
                startActivity(i)
                finish()
            }else{
                val i = Intent(this@SplashActivity, ChooseActivity::class.java)
                startActivity(i)
                finish()
            }

        }, 3000)
    }

    private fun setPrefernce() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences!!.edit()
    }

}