package com.seekex.partyregistration.interfaces

import android.net.Uri

interface ImageSelectionCallback {
    fun onImageSelect(uri: Uri)
}