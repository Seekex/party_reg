package com.seekx.utils

import android.app.ActivityManager
import android.content.Context
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import com.seekex.partyregistration.DashboardModel
import com.seekex.partyregistration.R
import java.util.*


class DataUtils {

    companion object {
        fun getVendorData(context: Context): ArrayList<DashboardModel>? {
            val dashboardModels: ArrayList<DashboardModel> = ArrayList<DashboardModel>()
            dashboardModels.add(
                DashboardModel(
                    context.getString(R.string.vendorreg),
                    context.getString(R.string.vendorreg),
                    android.R.drawable.star_on,
                    1
                )
            )
            dashboardModels.add(
                DashboardModel(
                    context.getString(R.string.vendorlist),
                    context.getString(R.string.vendorlist),
                    android.R.drawable.star_on,
                    2
                )
            )
            //        dashboardModels.add(new DashboardModel(context.getString(R.string.seeUser),context.getString(R.string.seeDetail),R.drawable.user_list,2));
            return dashboardModels
        }
        public fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
            val manager: ActivityManager =
                getSystemService(context, serviceClass) as ActivityManager
            for (service in manager.getRunningServices(Int.MAX_VALUE)) {
                if (serviceClass.name == service.service.getClassName()) {
                    Log.i("isMyServiceRunning?", true.toString() + "")
                    return true
                }
            }
            Log.i("isMyServiceRunning?", false.toString() + "")
            return false
        }


    }

}
