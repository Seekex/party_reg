package com.seekx.utils

import android.app.Dialog
import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.EditText

import java.util.regex.Matcher
import java.util.regex.Pattern


class ValidationUtils {

    companion object {

        fun isEmpty(editText: EditText): Boolean {
            if (editText.text.toString() == "")
                return true

            return false
        }

        fun haveCharacter(editText: EditText): Boolean {
            try{
                editText.text.toString().toLong()
            }catch (e: Exception){
                return true
            }
            return false
        }

        fun isAbove30(
            str: String
        ): Boolean {
            val matcher: Matcher = Pattern.compile("#(.+?) ", Pattern.DOTALL).matcher(str)
            while (matcher.find()) {
                if (matcher.group(1).length > 30)
                    return true
            }
            return false
        }


        fun isContainsSpace(editText: EditText): Boolean {
            if (editText.text.toString().contains(" "))
                return true

            return false
        }
        fun getErrorString(
            isEmptyEditText: Boolean,
            fromHour: Int,
            toHour: Int,
            fromMint: Int,
            toMint: Int
        ): String {
            Log.e(
                "checkForSlots",
                " fromHour  $fromHour   fromMint  $fromMint   toHour  $toHour  toMint  $toMint"
            )
            return when {
                fromHour==0 && isEmptyEditText -> {
                    "Please Select Start Time!"
                }
                toHour==0 && isEmptyEditText -> {
                    "Please Select End Time!"
                }
                (fromHour>toHour) || ((toHour==fromHour) && fromMint>toMint) ||  ((toHour==fromHour) && (fromMint==toMint))->{
                    "End time must be greater than Start Time."
                }
                else -> ""
            }
        }
        fun doesMatches(valueOne: String, valueTwo: String): Boolean {
            if (valueOne == valueTwo)
                return true

            return false
        }
        fun isValidEmail(target: CharSequence?): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
        fun isEmailValid(email: String): Boolean {

            var isValid = false

            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"

            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            if (matcher.matches()) {
                isValid = true
            }
            return isValid
        }

        fun isUpiCorrect(upi: String): Boolean {

            var isValid = false

            val expression = "([\\w.-]*[@][\\w]*)"

            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(upi)
            if (matcher.matches()) {
                isValid = true
            }
            return isValid
        }


        fun isMinimumLength(text: EditText, length: Int): Boolean {
            if (text.text.toString().length < length)
                return true

            return false
        }


        fun isValidMobileNo(mobile: String): Boolean {

            if (mobile.length < 10 || mobile.length > 13)
                return false
            else if (mobile.contains("+"))
                return false

            return true
        }
         fun isValidMobile(phone: String): Boolean {
            return if (!Pattern.matches("[a-zA-Z]+", phone)) {
                phone.length > 6 && phone.length <= 13
            } else false
        }
        fun isNullOrEmpty(value: String): Boolean {
            try {
                if (value == "")
                    return true
            } catch (e: NullPointerException) {
                return true
            }
            return false
        }



        fun checkBasics(rootView: View): Boolean {
            val context = rootView.context

            val dialogUtils = DialogUtils(context)
//            when {
//                isEmpty(rootView.et_name) -> {
//                    dialogUtils.showAlert(context.getString(R.string.err_name))
//                    return false
//                }
//                isEmpty(rootView.edt_email) -> {
//                    dialogUtils.showAlert(context.getString(R.string.err_email))
//                    return false
//                }
//
//                isEmpty(rootView.etGender) -> {
//                    dialogUtils.showAlert(context.getString(R.string.err_gender))
//                    return false
//                }
//
//                isEmpty(rootView.et_dob) -> {
//                    dialogUtils.showAlert(context.getString(R.string.error_dob))
//                    return false
//                }
//
//                isEmpty(rootView.et_pass) -> {
//                    dialogUtils.showAlert(context.getString(R.string.err_password))
//                    return false
//                }
//                isContainsSpace(rootView.et_pass) -> {
//                    dialogUtils.showAlert(context.getString(R.string.err_password_spaces))
//                    return false
//                }
//                isMinimumLength(rootView.et_pass, 6) -> {
//                    dialogUtils.showAlert(context.getString(R.string.err_valid_password))
//                    return false
//                }
//                isEmpty(rootView.et_cnf_pass) -> {
//                    dialogUtils.showAlert(context.getString(R.string.error_cnf_pass))
//                    return false
//                }
//                !isEmailValid(ExtraUtils.getVal(rootView.edt_email))->{
//                    dialogUtils.showAlert(context.getString(R.string.error_invalid_email))
//                    return false
//                }
//
//                !doesMatches(
//                    ExtraUtils.getVal(rootView.et_pass),
//                    ExtraUtils.getVal(rootView.et_cnf_pass)
//                ) -> {
//                    dialogUtils.showAlert(context.getString(R.string.error_match_pass))
//                    return false
//                }

//            }

            return true
        }




    }

}