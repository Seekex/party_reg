package com.seekex.scheduleproject.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import com.seekex.partyregistration.R
import org.json.JSONObject

class ActivityUtils {

    companion object {
        fun navigate(from: Activity, to: Class<*>, isBack: Boolean) {

            val intent = Intent(from, to)
            from.startActivity(intent)
            if (isBack)
                from.overridePendingTransition(
                    R.anim.move_left_in_activity,
                    R.anim.move_right_out_activity
                )
            else
                from.overridePendingTransition(
                    R.anim.move_right_in_activity,
                    R.anim.move_left_out_activity
                )
            from.finish()

        }

        fun navigateWithoutFinish(from: Activity, to: Class<*>) {
            val intent = Intent(from, to)
            from.startActivity(intent)
            from.overridePendingTransition(R.anim.activity_up, R.anim.no_change)
        }

        fun openActivity(from: Activity, to: Class<*>) {
            val intent = Intent(from, to)
            from.startActivity(intent)
        }



        fun isAccessibilityServiceEnabled(context: Context, accessibilityService: Class<*>?): Boolean {
            val expectedComponentName = ComponentName(context, accessibilityService!!)
            val enabledServicesSetting: String =
                Settings.Secure.getString(
                    context.contentResolver,
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES
                )
                    ?: return false
            val colonSplitter: TextUtils.SimpleStringSplitter = TextUtils.SimpleStringSplitter(':')
            colonSplitter.setString(enabledServicesSetting)
            while (colonSplitter.hasNext()) {
                val componentNameString: String = colonSplitter.next()
                val enabledService: ComponentName? =
                    ComponentName.unflattenFromString(componentNameString)
                if (enabledService != null && enabledService.equals(expectedComponentName)) return true
            }
            return false
        }

    }



}