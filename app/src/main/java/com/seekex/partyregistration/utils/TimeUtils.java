package com.seekex.partyregistration.utils;

import android.annotation.SuppressLint;
import android.util.Log;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class TimeUtils {

    public static String getTimestamp() {
        Long tsLong = System.currentTimeMillis();
        String timestamp = tsLong.toString();
        return timestamp;
    }

    public static String getDateAndTime(String pattern) {
        Date dateobj = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(dateobj);

    }


    public static boolean comparetowDates(String startDate, String endDate) {
        try {

            Log.v("compareTowDates", startDate + " " + endDate);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm"); //For declaring values in new date objects. use same date format when creating dates
            Date date1 = sdf.parse(startDate);
            Date date2 = sdf.parse(endDate);

            Log.v("compareTowDates", date1 + "\n" + date2);

            if (date2.before(date1)) {
                return false;
            }

        } catch (Exception e) {
            Log.v("comparetowDates", e.toString());
        }

        return true;

    }

    public static Long diffirenceDates(String dateStart, String dateStop, String patern) {

        //HH converts hour in 24 hours format (0-23), day calculation


        Date d1 = null;
        Date d2 = null;

        try {

            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(patern);

            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);


            long seconds = (diff / 1000);


            Log.v("diffirence", diffDays + " days, ");
            Log.v("diffirence", diffHours + " hours, ");
            Log.v("diffirence", diffMinutes + " minutes, ");
            Log.v("diffirence", diffSeconds + " seconds.");

            //   toast("diff "+diff);

            return seconds;

        } catch (Exception e) {

        }

        return 125L;
    }

    public static String timeStampToDate(long timestamp) {
        Date d = new Date(timestamp);
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String newDate = f.format(d);
        Log.v("timeStampToDate", newDate);
        return newDate;

    }

    public static String getLast(int lastDays) {
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -lastDays);
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        String newDate = f.format(cal.getTime());
        return newDate;
    }


    public static long convertDateToTimeStamp(String date) {
        long date1 = Date.parse(date);
        return date1;
    }

//    public static String formatDate(String strCurrentDate, String pattern) {
//        try {
//            SimpleDateFormat format = new SimpleDateFormat(Constants.ONLY_DATE);
//            Date newDate = format.parse(strCurrentDate);
//
//            format = new SimpleDateFormat(pattern);
//            String returnString = format.format((newDate));
//            Log.v("fromatDatedd", pattern + " " + returnString);
//            return returnString;
//        } catch (ParseException e) {
//            return strCurrentDate;
//        }
//    }

//    public static String formatDateForTask(String strCurrentDate, String pattern) {
//
//        try {
//            SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_TIME_PATTERN);
//            Date newDate = format.parse(strCurrentDate);
//
//            format = new SimpleDateFormat(pattern);
//            String returnString = format.format((newDate));
//
//            return returnString;
//        } catch (ParseException e) {
//            return strCurrentDate;
//        }
//    }

    public static String dateFormater(String dateFromJSON, String expectedFormat, String oldFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date date = null;
        String convertedDate = null;
        try {
            date = dateFormat.parse(dateFromJSON);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(expectedFormat);
            convertedDate = simpleDateFormat.format(date);
        } catch (Exception e) {
            Log.v("dateFormater", "" + e.toString());
        }
        Log.v("dateFormater", "" + convertedDate);
        return convertedDate;
    }

//    public static Date getDate(String currentDate) {
//        try {
//            return new SimpleDateFormat(Constants.DATE_TIME_PATTERN).parse(currentDate);
//        } catch (ParseException e) {
//            Log.v("dateFormater", "getDate " + e.toString());
//            return new Date();
//        }
//    }

    public static String convertSecondToHHMMString(Long count) {
        SimpleDateFormat df = new SimpleDateFormat("mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(new Date(count * 1000L));
    }
}
