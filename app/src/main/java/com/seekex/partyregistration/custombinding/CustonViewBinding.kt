package com.seekex.scheduleproject.custombinding

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.SystemClock
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.seekex.partyregistration.R
import com.seekx.utils.DataUtils
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ImageUtils
import org.apache.commons.lang3.StringUtils
import org.ocpsoft.prettytime.PrettyTime
import java.util.*
import kotlin.collections.ArrayList

class CustonViewBindings {

    companion object {

        @JvmStatic
        @BindingAdapter("uriImage")
        fun uriImage(view: ImageView?, uriImage: String?) {
            Log.e("TAG", "uriImage: ", )
            if (view == null || uriImage == null)
                return
            Log.e("TAG", "uriImage: "+uriImage )

            view.setImageURI(Uri.parse(uriImage))

//            view.setOnClickListener {
////                ImageUtils.showImageUri(Uri.parse(uriImage), view.context)
//            }

        }



        @JvmStatic
        @BindingAdapter("choronmeterSetting")
        fun choronmeterSetting( ch_meter: Chronometer?,timieinmillis: String?) {


            try {
                if (timieinmillis!!.equals("")||timieinmillis == null || ch_meter == null)
                    return
// 60*1000*2  2 mint
                val timeInMil = 60*60*1000  // 1 hour
                val timeInMilSeconds = System.currentTimeMillis()-timieinmillis.toLong()
//        Log.e("", "setListener: "+System.currentTimeMillis() +" -- "+SystemClock.elapsedRealtime())
//        Log.e("", "timeInMilSeconds: "+timeInMilSeconds)

                ch_meter.base = SystemClock.elapsedRealtime() - timeInMilSeconds
                ch_meter.start()
            } catch (e: Exception) {
            }

        }
        @JvmStatic
        @BindingAdapter("openImage")
        fun openImage(view: ImageView?, image: String?) {
            if (view == null || image == null)
                return

            try {
                Glide.with(view.context)
                    .load(image)
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(view)

                view.setOnClickListener {
                    ImageUtils.showImage(image, view.context)
                }
            } catch (e: Exception) {
            }
        }


        @JvmStatic
        @BindingAdapter("closedImage")
        fun closedImage(view: ImageView?, image: String?) {

            try {
                if (view == null || image == null)
                    return

                Glide.with(view.context)
                    .load(image)
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(view)
            } catch (e: Exception) {
            }
        }


        @JvmStatic
        @BindingAdapter("capitalizeText")
        fun capitalizeText(view: TextView?, str: String?) {

            if (view == null || str == null)
                return


            view.text= ExtraUtils.capitalizeText(str)
            view.setEllipsize(TextUtils.TruncateAt.MARQUEE)
            view.isSelected=true

        }



        fun setDrawableImage(view: RoundedImageViw?, drawableId: Drawable){
            try {
                Glide.with(view!!.context)
                    .load(drawableId)
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(view)
            } catch (e: Exception) {
            }
        }









//        fun setIntAndExpert(view: TextView?, expert: Boolean?) {
//
//            if (expert!!)
//                view?.text = view?.context?.getString(R.string.expertise)
//            else
//                view?.text = view?.context?.getString(R.string.interest)
//
//        }

    }


}
